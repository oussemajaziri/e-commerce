export class Basket {

    id! : number;
    user!: any;
    basketItems!: any[];
    createdDate!: Date;
    modifiedDate!: Date;
    checkedOut!: boolean;
    comment!:string;
}