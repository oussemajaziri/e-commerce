import { AppComponent } from './app.component';

describe('AppComponent', () => {

  let fixture: AppComponent;
  beforeEach(async () => {
    fixture = new AppComponent();
  });

  it('should create the app', () => {
    expect(fixture).toBeTruthy();
  });

  it(`should have as title 'ecommerce-Front'`, () => {
    expect(fixture.title).toEqual('ecommerce-Front');
  });

  it('should render title', () => {
    const compiled = document.createElement('div');
    compiled.innerHTML = '<div class="content"><span>ecommerce-Front app is running!</span></div>';
    expect(compiled.querySelector('.content span')?.textContent).toContain('ecommerce-Front app is running!');
  });

});
