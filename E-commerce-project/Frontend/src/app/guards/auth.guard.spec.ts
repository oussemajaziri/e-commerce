import { AuthGuard } from './auth.guard';

describe('AuthGuard', () => {
  let guard: AuthGuard;
  let authenticationServiceMock : any;
  let routerMock : any;

  beforeEach(() => {
    authenticationServiceMock = {
      getToken : jest.fn()
    };
    routerMock = {
      navigateByUrl : jest.fn()
    }
    guard = new AuthGuard(authenticationServiceMock,routerMock);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

  it('should canActivate token case', () => {
    const token = jest.spyOn(authenticationServiceMock,'getToken').mockReturnValue('faketoken');
    let x :any;
    let y : any;
    guard.canActivate(x,y);
    expect(guard).toBeTruthy();
  });

  it('should canActivate no token case', () => {
    const token = jest.spyOn(authenticationServiceMock,'getToken').mockReturnValue(null);
    let x :any;
    let y : any;
    guard.canActivate(x,y);
    expect(guard).toBeTruthy();
  });


});
