import { RolesGuard } from './roles.guard';

describe('RolesGuard', () => {
  let guard: RolesGuard;

  let authenticationServiceMock : any;
  let routerMock : any;

  beforeEach(() => {
    authenticationServiceMock = {
      getRoles : jest.fn()
    };
    routerMock = {
      navigateByUrl : jest.fn()
    }
    guard = new RolesGuard(authenticationServiceMock,routerMock);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

  it('should canActivate token case', () => {
    const token = jest.spyOn(authenticationServiceMock,'getRoles').mockReturnValue('ADMIN');
    let x :any={
      data : {
        role : 'ADMIN'
      }
    };
    let y : any;
    guard.canActivate(x,y);
    expect(guard).toBeTruthy();
  });

  it('should canActivate no token case', () => {
    const token = jest.spyOn(authenticationServiceMock,'getRoles').mockReturnValue('USER');
    let x :any={
      data : {
        role : 'ADMIN'
      }
    };
    let y : any;
    guard.canActivate(x,y);
    expect(guard).toBeTruthy();
  });
});
