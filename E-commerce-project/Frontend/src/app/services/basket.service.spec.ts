import { TestBed } from '@angular/core/testing';

import { BasketService } from './basket.service';

describe('BasketService', () => {
  let service: BasketService;
  let httpClientSpy: any;

  beforeEach(() => {
    httpClientSpy = {
      get: jest.fn(),
      post: jest.fn()
    }
    service = new BasketService(httpClientSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should test addBasket', () => {
    let basket: any;
    service.addBasket(basket);
    expect(httpClientSpy.post).toHaveBeenCalled();
  });

  it('should test getUserBaskets', () => {
    let userEmail: string = 'email';
    service.getUserBaskets(userEmail);
    expect(httpClientSpy.get).toHaveBeenCalled();
  });

  it('should test getAllBaskets', () => {
    service.getAllBaskets();
    expect(httpClientSpy.get).toHaveBeenCalled();
  });

  it('should test validateBasket', () => {
    let basketId = 1;
    service.validateBasket(basketId);
    expect(httpClientSpy.post).toHaveBeenCalled();
  });

});
