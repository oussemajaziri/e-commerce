import { HttpClient, HttpResponse } from '@angular/common/http';
import { JwtHelperService } from "@auth0/angular-jwt";
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { tap } from "rxjs/operators";


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  URL = "http://localhost:8080/api/v1";
  private jwtHelper = new JwtHelperService();
  token!: string;
  roles!: any;
  userEmail: any;
  isAdminConnected = new BehaviorSubject<boolean>(false);
  isUserConnected = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient) { 
    this.token = localStorage.getItem('token') || '';
  }

  register(user: any) {
    return this.http.post(`${this.URL}/auth/register`, user);
  }

  authenticate(user: any): Observable<HttpResponse<any>> {

    return this.http.post(`${this.URL}/auth/authenticate`, user, { observe: 'response' }).pipe(
      tap(res => {
        this.token = JSON.stringify((res.body as any).token);
        localStorage.setItem('token', this.token);
        this.roles = this.jwtHelper.decodeToken(this.token).roles;
        localStorage.setItem('role', this.roles[0].authority);
        this.userEmail = this.jwtHelper.decodeToken(this.token).sub;
        localStorage.setItem('username', this.userEmail);
        this.checkIfAdminIsConnected();
      })
    )

  }

  public logOut(): void {

    this.token = "";
    this.roles = null;
    this.userEmail = "";

    this.isAdminConnected.next(false);
    this.isUserConnected.next(false);

    localStorage.removeItem('token');
    localStorage.removeItem('role');
    localStorage.removeItem('username');

  }

  getConnectUser(): Observable<any> {
    let user: Observable<any>;
    let userEmail = localStorage.getItem('username');
    if (userEmail != null) {
      user = this.http.get(`${this.URL}/users/username/${userEmail}`);
      return user;
    } else {
      user = of(null);
      return user;
    }
  }

  checkIfAdminIsConnected() {
    let role = localStorage.getItem('role');
    if (role?.toString() == 'ADMIN') {
      this.isAdminConnected.next(true);
    } else if (role?.toString() == 'USER') {
      this.isUserConnected.next(true);
    }
    else {
      this.isAdminConnected.next(false);
      this.isUserConnected.next(false);
    }
  }

  getToken(): string {
    return this.token;
  }

  getRoles(): any {
    this.roles = localStorage.getItem('role');
    return this.roles;
  }

}
