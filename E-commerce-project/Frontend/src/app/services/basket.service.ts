import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Basket } from '../models/basket.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BasketService {

  URL = "http://localhost:8080/api/v1";

  constructor(private http:HttpClient) { }

  addBasket(basket : Basket):Observable<any>{
    return this.http.post(`${this.URL}/baskets`,basket);
  }

  getUserBaskets(userEmail:string):Observable<Basket[]>{
    return this.http.get<Basket[]>(`${this.URL}/baskets/${userEmail}`);
  }

  getAllBaskets():Observable<Basket[]>{
    return this.http.get<Basket[]>(`${this.URL}/baskets`);
  }

  validateBasket(basketId:number):Observable<Basket>{
    return this.http.post<Basket>(`${this.URL}/baskets/${basketId}`,null);
  }

  refuseBasket(basketId:number, comment:String):Observable<Basket>{
    return this.http.post<Basket>(`${this.URL}/baskets/${comment}/${basketId}`,null);
  }

}