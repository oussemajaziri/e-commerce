import { AuthenticationService } from './authentication.service';
import { HttpResponse } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { of } from 'rxjs';


describe('AuthenticationService', () => {
  let service: AuthenticationService;
  let httpClientSpy: any;

  beforeEach(() => {
    httpClientSpy = {
      post: jest.fn(() => ({
        pipe: jest.fn()
      })),
      get: jest.fn()
    };
    service = new AuthenticationService(httpClientSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('shoul test register', () => {
    let user: any;
    service.register(user);
    expect(httpClientSpy.post).toHaveBeenCalled();
  });

  it('shoul test authenticate', () => {
    let user: any;
    service.authenticate(user);
    expect(httpClientSpy.post).toHaveBeenCalled();
  });


  it('should test authenticate with token', () => {
    const jwtHelper = new JwtHelperService();
    const fakeToken = 'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJqYXpAbWFpbC5jb20iLCJyb2xlcyI6W3siYXV0aG9yaXR5IjoiQURNSU4ifV0sImlhdCI6MTY5MjAwMjgwMSwiZXhwIjoxNjkyMDIwODAxfQ.hGHg8L56B1geJoJG5AXxK1kkdK4ETdOgJryi_-zk568';
    const user: any = {
      "firstName":'ouss',
      "lastName":'jaz',
      "email":'jaz@mail.com',
      "password":'123',
      "phone":'123',
      "role":'ADMIN'
    };
    const mockResponse: HttpResponse<any> = new HttpResponse({
      body: { token: fakeToken }
    });

    httpClientSpy.post.mockReturnValue(of(mockResponse));

    service.authenticate(user).subscribe(() => {

      expect(service.token).toEqual(JSON.stringify(fakeToken));
      expect(localStorage.getItem('token')).toEqual(JSON.stringify(fakeToken));

      const decodedToken = jwtHelper.decodeToken(fakeToken);
      expect(service.roles).toEqual(decodedToken.roles);
      expect(localStorage.getItem('role')).toEqual(decodedToken.roles[0].authority);

      expect(service.userEmail).toEqual(decodedToken.sub);
      expect(localStorage.getItem('username')).toEqual(decodedToken.sub);

      expect(localStorage.setItem('role',service.roles[0].authority)).toHaveBeenCalled();
      expect(service.userEmail).toBeDefined();

      expect(service.isAdminConnected.value).toEqual(true);
    });

    expect(httpClientSpy.post).toHaveBeenCalled();
  });

  it('shoul test logOut', () => {
    service.logOut();
    service.isAdminConnected.subscribe(isAdminConnected => {
      expect(isAdminConnected).toBeFalsy();
    });
  });

  it('should test getConnectUser no user case', () => {
    const spyGetItem = jest.spyOn(Storage.prototype, 'getItem');
    spyGetItem.mockReturnValue(null);
    let user: any;
    service.getConnectUser().subscribe(
      (data) => {
        user = data;
        expect(user).toBeNull();
      }
    );
  });


  it('should test getConnectUser user case', () => {
    const spyGetItem = jest.spyOn(Storage.prototype, 'getItem');
    spyGetItem.mockReturnValue('user@mail.com');
    const spyGet = jest.spyOn(httpClientSpy, 'get');
    spyGet.mockReturnValue('mail@mail.com');
    service.getConnectUser();
    expect(httpClientSpy.get).toHaveBeenCalled();
  });

  it('should test checkIfAdminIsConnected ADMIN case', () => {
    const spyGetItem = jest.spyOn(Storage.prototype, 'getItem');
    spyGetItem.mockReturnValue('ADMIN');
    service.checkIfAdminIsConnected()
    service.isUserConnected.subscribe(isUserConnected => {
      expect(isUserConnected).toBeFalsy();
    });
    service.isAdminConnected.subscribe(isAdminConnected => {
      expect(isAdminConnected).toBeTruthy();
    });
  });


  it('should test checkIfAdminIsConnected USER case', () => {
    const spyGetItem = jest.spyOn(Storage.prototype, 'getItem');
    spyGetItem.mockReturnValue('USER');
    service.checkIfAdminIsConnected()
    service.isUserConnected.subscribe(isUserConnected => {
      expect(isUserConnected).toBeTruthy();
    });
    service.isAdminConnected.subscribe(isAdminConnected => {
      expect(isAdminConnected).toBeFalsy();
    });
  });


  it('should test checkIfAdminIsConnected no connected user case', () => {
    const spyGetItem = jest.spyOn(Storage.prototype, 'getItem');
    spyGetItem.mockReturnValue(null);
    service.checkIfAdminIsConnected()
    service.isUserConnected.subscribe(isUserConnected => {
      expect(isUserConnected).toBeFalsy();
    });
    service.isAdminConnected.subscribe(isAdminConnected => {
      expect(isAdminConnected).toBeFalsy();
    });
  });


  it('should test getToken', () => {
    service.getToken()
    expect(service.token).toBeDefined();
  });

  it('should test getRoles', () => {
    service.getRoles()
    expect(service.roles).toBeDefined();
  });

});
