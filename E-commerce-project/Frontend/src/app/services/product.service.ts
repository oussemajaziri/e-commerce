import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  URL = "http://localhost:8080/api/v1";

  constructor(private http : HttpClient) { }

  addProduct(prod : FormData){
    return this.http.post(`${this.URL}/admin/products`,prod);
  }

  updateProduct(prod : FormData){
    return this.http.post(`${this.URL}/admin/products/update`,prod);
  }

  getAllProducts(page:number):Observable<any>{
    return this.http.get(`${this.URL}/products/page/${page}`);
  }

  getProductById(prodId:number):Observable<any>{
    return this.http.get(`${this.URL}/products/${prodId}`);
  }

  getProductImage(imgName:string):Observable<any>{
    return this.http.get(`${this.URL}/products/images/${imgName}`);
  }
 

}
