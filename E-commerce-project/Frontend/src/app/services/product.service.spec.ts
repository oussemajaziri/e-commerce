import { ProductService } from './product.service';

describe('ProductService', () => {
  let service: ProductService;
  let httpClientSpy :any;
  
  beforeEach(() => {
    httpClientSpy = {
      get : jest.fn(),
      post : jest.fn()
    };
    service = new ProductService(httpClientSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should test addProduct', () => {
    let prod : any;
    service.addProduct(prod);
    expect(httpClientSpy.post).toBeCalledWith(`http://localhost:8080/api/v1/admin/products`,prod)
  });

  it('should test updateProduct', () => {
    let prod : any;
    service.updateProduct(prod);
    expect(httpClientSpy.post).toBeCalledWith(`http://localhost:8080/api/v1/admin/products/update`,prod)
  });


  it('should test getAllProducts', ()=> {
    const page = 0;
    service.getAllProducts(page);
    expect(httpClientSpy.get).toBeCalledTimes(1);
  });


  it('should test getProductById', () => {
    const id = 1;
    service.getProductById(id);
    expect(httpClientSpy.get).toHaveBeenCalled();
  });


  it('should test getProductImage', () => {
    const img : string = "image";
    service.getProductImage(img);
    expect(httpClientSpy.get).toBeCalledTimes(1);
  })

});
