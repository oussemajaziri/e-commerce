import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddProductComponent } from './components/add-product/add-product.component';
import { EditProductComponent } from './components/edit-product/edit-product.component';
import { HomeComponent } from './components/home/home.component';
import { ProductsComponent } from './components/products/products.component';
import { SignupComponent } from './components/signup/signup.component';
import { LoginComponent } from './components/login/login.component';
import { TemporaryBasketComponent } from './components/temporary-basket/temporary-basket.component';
import { UsersBasketsComponent } from './components/users-baskets/users-baskets.component';
import { AllBasketsComponent } from './components/all-baskets/all-baskets.component';
import { AuthGuard } from './guards/auth.guard';
import { Error403Component } from './components/error403/error403.component';
import { RolesGuard } from './guards/roles.guard';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'product/:id', component: EditProductComponent, canActivate: [AuthGuard,RolesGuard], data: {role: 'ADMIN'}},
  {path: 'product', component: AddProductComponent, canActivate: [AuthGuard,RolesGuard], data: {role: 'ADMIN'}},
  {path: 'products', component: ProductsComponent},
  {path: 'signup', component: SignupComponent},
  {path: 'login', component: LoginComponent},
  {path: 'temporarybasket', component: TemporaryBasketComponent},
  {path: 'mybaskets', component: UsersBasketsComponent, canActivate: [AuthGuard,RolesGuard], data: {role: 'USER'}},
  {path: 'allbaskets', component: AllBasketsComponent, canActivate: [AuthGuard,RolesGuard], data: {role: 'ADMIN'}},
  {path: 'page-error-403', component: Error403Component}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
