import { HomeComponent } from './home.component';
import { AuthenticationService } from 'src/app/services/authentication.service';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let auth : AuthenticationService;

  beforeEach(() => {
    component = new HomeComponent(auth);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
