import { of } from 'rxjs';
import { AllBasketsComponent } from './all-baskets.component';
import { Basket } from 'src/app/models/basket.model';
import Swal, { SweetAlertResult } from 'sweetalert2';

describe('AllBasketsComponent', () => {
  let component: AllBasketsComponent;
  let basketService : any;
  let auth : any;

  beforeEach(() => {
    basketService = {
      getAllBaskets : jest.fn(()=>of(new Basket()))
    }
    component = new AllBasketsComponent(basketService,auth)
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should ngOnInit', () => {
    component.ngOnInit();
    expect(component.baskets).toBeDefined();
  });

  it('should validateBasket', async () => {
    const SwalFireMock = jest.spyOn(Swal, 'fire').mockResolvedValue({
      isConfirmed: true,
    } as SweetAlertResult);
  
    const SwalMixinMock = jest.spyOn(Swal, 'mixin').mockReturnValue({
      fire: jest.fn(),
    } as any); 
    let basket = new Basket;
    component.validateBasket(basket);
  
    // Attendre la résolution de la promesse dans la méthode logOut
    await Promise.resolve();
  
    expect(SwalFireMock).toHaveBeenCalled();
  
    expect(SwalMixinMock).toHaveBeenCalled();

  });

  it('should not validateBasket', async () => {
    const SwalFireMock = jest.spyOn(Swal, 'fire').mockResolvedValue({
      isDenied: true,
    } as SweetAlertResult);
    let b = new Basket;
    component.validateBasket(b);
  
    // Attendre la résolution de la promesse dans la méthode logOut
    await Promise.resolve();
  
    expect(SwalFireMock).toHaveBeenCalled();

  });

  it('should showDetails', () => {
    let basket = new Basket();
    basket = {
        id : 1,
        user: {id:1},
        basketItems: [{product:1,quantity:2}],
        createdDate: new Date(),
        modifiedDate: new Date(),
        checkedOut: false,
        comment:''
    }
    component.showDetails(basket,'show');
    expect(component.basketToShowDetails).toBeDefined();
    expect(component.basketToShowTotalPrice).toBeDefined();
  });


  it('should hideDetails', () => {
    component.hideDetails();
    expect(component.basketToShowDetails).toBeNull();
    expect(component.basketToShowTotalPrice).toBe(0);
  });


});
