import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Basket } from 'src/app/models/basket.model';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { BasketService } from 'src/app/services/basket.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-all-baskets',
  templateUrl: './all-baskets.component.html',
  styleUrls: ['./all-baskets.component.css']
})
export class AllBasketsComponent implements OnInit {

  baskets!: Basket[];
  basketToShowDetails: any = null;
  basketToShowTotalPrice!: number;
  canRefuse: boolean = false;
  comment!: string;


  constructor(private basketService: BasketService, private auth: AuthenticationService) { }


  ngOnInit(): void {
    this.basketService.getAllBaskets().subscribe(
      (data) => {
        this.baskets = data;
      }
    )
  }

  validateBasket(basket: Basket) {
    Swal.fire({
      title: '<small>Voulez-vous valider cette commande ?</small>',
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonColor: '#17a2b8',
      cancelButtonText: 'ANNULER',
      confirmButtonText: 'VALIDER',
      denyButtonText: `REFUSER`,
    }).then((result) => {

      if (result.isConfirmed) {
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 750,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        })
        Toast.fire({
          icon: 'success',
          title: 'commande validée',
        }).then(() => {
          this.basketService.validateBasket(basket.id).subscribe(
            (data) => {
              this.basketService.getAllBaskets().subscribe(
                (data) => {
                  this.baskets = data;
                }
              )
            }
          )
        })
      } else if (result.isDenied) {

        this.showDetails(basket, "refuse");

      } else {
        //alert('ANNULATION')
      }
    })
  }



  showDetails(basket: Basket, showOrRefuse: 'show' | 'refuse') {
    if (showOrRefuse == 'refuse') {
      this.canRefuse = true;
    }
    this.basketToShowDetails = basket;
    this.basketToShowTotalPrice = 0;
    for (let i = 0; i < this.basketToShowDetails.basketItems.length; i++) {
      this.basketToShowTotalPrice += this.basketToShowDetails.basketItems[i].product.productPrice * this.basketToShowDetails.basketItems[i].quantity;
    }
  }


  hideDetails() {
    this.basketToShowDetails = null;
    this.basketToShowTotalPrice = 0;
    this.canRefuse = false;
    this.comment = '';
  }

  refuseBasket(basketId:number) {
    this.basketService.refuseBasket(basketId, this.comment).subscribe({
      next : (data) => {
        this.basketService.getAllBaskets().subscribe(
          (data) => {
            this.baskets = data;
          }
        )
      },
      complete: ()=>{
        this.basketToShowDetails = null;
        this.basketToShowTotalPrice = 0;
        this.canRefuse = false;
        this.comment = '';
      }
    }
    )
  }

}
