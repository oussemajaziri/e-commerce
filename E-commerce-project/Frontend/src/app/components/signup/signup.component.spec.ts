import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignupComponent } from './signup.component';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

describe('SignupComponent', () => {
  let component: SignupComponent;
  let fbMock : FormBuilder;
  let authenticationServiceMock : any;
  let routerMock : any;

  beforeEach(() => {
    fbMock = new FormBuilder();
    const user : any = {
      firstName: 'ouss',
      lastName : 'jaz'
    }
    authenticationServiceMock = {
      register: jest.fn(()=> of(user))
    };
    routerMock = {
      navigateByUrl : jest.fn()
    };
    component = new SignupComponent(fbMock,authenticationServiceMock,routerMock);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should ngOnInit', () => {
    component.ngOnInit();
    expect(component.signupForm).toBeDefined();
  });

  it('should register', () => {
    component.ngOnInit();
    component.register();
    expect(routerMock.navigateByUrl).toHaveBeenCalled();
  });
});
