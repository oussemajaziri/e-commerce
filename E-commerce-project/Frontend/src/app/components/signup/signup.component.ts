import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signupForm! : FormGroup;

  constructor(private fb : FormBuilder , private authenticationService : AuthenticationService, private router:Router) { }

  ngOnInit(): void {
    this.signupForm=this.fb.group({
      "firstName":'',
      "lastName":'',
      "email":'',
      "password":'',
      "phone":'',
      "role":'USER'
    })
  }

  register(){
    this.authenticationService.register(this.signupForm.value).subscribe(
      data => {
        //console.log("User :",data);
        this.router.navigateByUrl('login');
      }
    )
  }

}
