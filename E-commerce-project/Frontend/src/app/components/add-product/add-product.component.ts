import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  productForm!: FormGroup;
  selectedImg!: File;
  imagePreview: any;
  product: any;


  constructor(private fb: FormBuilder, private productService: ProductService, private router :Router) { }

  ngOnInit(): void {

    this.productForm = this.fb.group({
      productName: '',
      productQuantity: '',
      productPrice: ''
    })

  }

  onFileSelected(img: any) {
    this.selectedImg = img.target.files[0];
    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = reader.result as string
    };
    reader.readAsDataURL(this.selectedImg);
  }

  addProduct() {
    const formData = new FormData();
    formData.append('productName', this.productForm.value.productName);
    formData.append('productQuantity', this.productForm.value.productQuantity);
    formData.append('productPrice', this.productForm.value.productPrice);
    formData.append('productImage', this.selectedImg);

    this.productService.addProduct(formData).subscribe(
      (data) => {this.product = data;
      this.router.navigateByUrl("products");}
    )
      
  }

}
