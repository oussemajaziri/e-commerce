import { AddProductComponent } from './add-product.component';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { of } from 'rxjs';

describe('AddProductComponent', () => {
  let component: AddProductComponent;
  let fbMock: FormBuilder;
  let productServiceMock: any;
  let routerMock: Router;

  beforeEach( () => {
    fbMock = new FormBuilder();
    productServiceMock = {
      addProduct: jest.fn(() => of({
        productName: 'prod-1',
        productQuantity: '5',
        productPrice: '10'
        }
      ))
    }
    component = new AddProductComponent(fbMock, productServiceMock, routerMock);
  });

  afterEach(() => {
    jest.clearAllMocks()
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize the form', () => {
    component.ngOnInit();
    expect(component.productForm).toBeDefined();
  });

  it('should select img', async () => {
    const img = {
      target: {
        files: [new Blob()],
      },
    };
  
    component.onFileSelected(img);
  
    // Attendez que le chargement de l'image soit terminé
    await new Promise((resolve) => {
      setTimeout(resolve, 100); // Attendez un court instant (ajustez si nécessaire)
    });
  
    expect(component.selectedImg).toEqual(img.target.files[0]);
    expect(component.imagePreview).toBeDefined()
  });

  it('should add product', () => {
    component.ngOnInit();
    component.selectedImg = new File([''], 'image.jpeg');
    component.productForm.setValue({
      productName: 'prod-1',
      productQuantity: '5',
      productPrice: '10'
    });
    component.addProduct();
    expect(productServiceMock.addProduct).toHaveBeenCalled();
  });

});
