import { BehaviorSubject, of } from 'rxjs';
import { ProductsComponent } from './products.component';

describe('ProductsComponent', () => {
  let component: ProductsComponent;
  let productServiceMock : any;
  let basketServiceMock : any;
  let authenticationServiceMock : any;
  let routerMock : any;

  beforeEach(() => {
    let prods = [{
      id:1,
      productName:'p1',
      productQuantity:2
    },
    {
      id:2,
      productName:'p2',
      productQuantity:3
    }]
    productServiceMock = {
      getAllProducts: jest.fn(() => of(prods))
    };
    authenticationServiceMock = {
      checkIfAdminIsConnected : jest.fn(),
      isUserConnected : new BehaviorSubject<boolean>(false),
      isAdminConnected : new BehaviorSubject<boolean>(false)
    };
    routerMock = {
      navigateByUrl:jest.fn()
    }
    component = new ProductsComponent(productServiceMock,basketServiceMock,authenticationServiceMock,routerMock);
  });

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should ngOnInit', () => {
    component.ngOnInit();
    expect(authenticationServiceMock.checkIfAdminIsConnected).toHaveBeenCalled();
  });

  it('should previousPage', () => {
    component.previousPage();
    expect(component.page).toBe(-1);
    expect(productServiceMock.getAllProducts).toHaveBeenCalled();
  });

  it('should nextPage', () => {
    component.nextPage();
    expect(component.page).toBe(1);
    expect(productServiceMock.getAllProducts).toHaveBeenCalled();
  });

  it('should addToBasket', () => {
    component.ngOnInit();
    component.addToBasket(2, 1);
    expect(component.showMessage).toBeFalsy();
  });

  it('should addToTemporaryBasket no basket case', () => {
    const localStorageMock = (function () {
      let store : any  = {};
    
      return {
        getItem(key:any) {
          return store[key];
        },
    
        setItem(key:any, value:any) {
          store[key] = value;
        },
    
        clear() {
          store = {};
        },
    
        removeItem(key:any) {
          delete store[key];
        },
    
        getAll() {
          return store;
        },
      };
    })();

    const setLocalStorage = (id:any) => {
      window.localStorage.getItem(id);
    };

    const mockId = "basket";
    setLocalStorage(mockId);

    Object.defineProperty(window, "localStorage", { value: localStorageMock });


    component.ngOnInit();
    component.addToTemporaryBasket(2, 1);
    const updatedBasket = JSON.parse(localStorageMock.getItem('basket'));
    expect(updatedBasket).toEqual([{ id: 2, quantity: 1 }]);
    component.addToTemporaryBasket(20, 10);
    const updatedBasket2 = JSON.parse(localStorageMock.getItem('basket'));
    console.log('updatedBasket ========= ',updatedBasket);
    
    // Utilisation de expect.arrayContaining pour ignorer l'ordre des éléments
    expect(updatedBasket2).toEqual(
      expect.arrayContaining([
        {"id": 2, "quantity": 1},
        {"id": 20, "quantity": 10}
      ])
    );
  });
  
  it('should editProduct', () => {
    component.ngOnInit();
    component.editProduct(2);
    expect(routerMock.navigateByUrl).toHaveBeenCalledWith(`product/2`); 
  });

  it('should closeMessageWindow', () => {
    component.closeMessageWindow();
    expect(component.showMessage ).toBeFalsy(); 
  });

});
