import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { BasketService } from 'src/app/services/basket.service';
import { ProductService } from 'src/app/services/product.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  page: number = 0;
  products: any = [];
  nbrPages!: number;
  buttonActive: boolean = true;

  isAdminConnected: boolean = false;
  isAdminConnectedSubscription!: Subscription;


  showMessage!: boolean;
  message!: string;


  constructor(private productService: ProductService, private basketService: BasketService, private authenticationService: AuthenticationService, private router: Router) { }

  ngOnInit(): void {
    this.authenticationService.checkIfAdminIsConnected();
    this.isAdminConnectedSubscription = this.authenticationService.isAdminConnected.subscribe((val) => {
      this.isAdminConnected = val
    }
    );
    this.productService.getAllProducts(this.page).subscribe(
      data => {
        this.nbrPages = data.totalPages;
        this.products = data.content;
      }
    )
  }

  previousPage() {
    this.page -= 1;
    this.productService.getAllProducts(this.page).subscribe(
      data => {
        this.products = data.content;
      }
    )
    document.documentElement.scrollTop = 0;
  }

  nextPage() {
    this.page += 1;
    this.productService.getAllProducts(this.page).subscribe(
      data => {
        this.products = data.content;
      }
    )
    document.documentElement.scrollTop = 0;
  }

  addToBasket(id: number, availableQuantity: number) {

    let localBasket = localStorage.getItem('basket');
    let temporaryBasket: any[] = [];
    if (localBasket != null) {
      temporaryBasket = JSON.parse(localBasket);
    }
    let idLists: number[] = [];
    if (temporaryBasket.length > 0) {
      for (let i = 0; i < temporaryBasket.length; i++) {
        idLists.push(temporaryBasket[i].id);
      }
    }

    if (idLists.includes(id)) {
      this.showMessage = true;
      this.message = "Vous avez déja selectionné ce produit !";
      //alert("Vous avez déja selectionné ce produit !")
    } else {
      let quantities: number[] = [];

      for (let i = 0; i < availableQuantity; i++) {
        quantities.push(i + 1);
      }
      let quantityOptions = quantities.map(q => `<option>${q}</option>`).join('');
      Swal.fire({
        title: '<small>Selectionner la quantité souhaitée</small>',
        html:
          `<select id="quantitySelect"> ` +
          quantityOptions +
          `</select>`,
        showDenyButton: false,
        showCancelButton: true,
        confirmButtonColor: '#17a2b8',
        cancelButtonText: 'Annuler',
        confirmButtonText: 'Confirmer',
        denyButtonText: `Non`,
        didOpen: () => {
          const selectElement = document.getElementById('quantitySelect');
          if (selectElement) {
            selectElement.style.cursor = 'pointer';
            selectElement.style.minWidth = '3.5rem';
            selectElement.style.minHeight = '1.75rem';
            selectElement.style.textAlign = 'center';
          }
        }
      }).then((result) => {

        if (result.isConfirmed) {
          const quantitySelect = document.getElementById('quantitySelect') as HTMLSelectElement;
          const selectedQuantity = quantitySelect ? parseInt(quantitySelect.value) : 0;
          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 1000,
            timerProgressBar: true,
            didOpen: (toast) => {
              toast.addEventListener('mouseenter', Swal.stopTimer);
              toast.addEventListener('mouseleave', Swal.resumeTimer);
              this.buttonActive = false;
            }
          })
          Toast.fire({
            icon: 'success',
            title: 'Le produit a été ajouté au panier',
          }).then(() => {
            //this.router.navigateByUrl('menu');
            this.addToTemporaryBasket(id, selectedQuantity);
            this.buttonActive = true;
          })
        } else if (result.isDenied) {
          //this.router.navigateByUrl('admin/add-menu');
        }
      })
    }
  }


  addToTemporaryBasket(prodId: number, quantity: number) {
    if (localStorage.getItem('basket') == null) {
      let basketItems: any[] = [];
      let basketItem = { id: prodId, quantity: quantity };
      basketItems.push(basketItem);
      let basketData = JSON.stringify(basketItems);
      localStorage.setItem('basket', basketData);
    } else {
      let basketItems: any[] = [];
      const storedBasket = localStorage.getItem('basket');
      console.log('storedBasket ........',storedBasket);
      if (storedBasket !== null) {
        basketItems = JSON.parse(storedBasket);
      }
      console.log('storedBasket ........',storedBasket);
      basketItems.push({ id: prodId, quantity: quantity });
      let basketData = JSON.stringify(basketItems);
      localStorage.removeItem('basket');
      localStorage.setItem('basket', basketData);
      console.log('basketData :::::',basketData);
    }
  }


  editProduct(id: number) {
    this.router.navigateByUrl(`product/${id}`)
  }

  closeMessageWindow() {
    this.showMessage = false;
  }

}
