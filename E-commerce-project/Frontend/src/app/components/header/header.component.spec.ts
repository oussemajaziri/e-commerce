import Swal, { SweetAlertResult } from 'sweetalert2';
import { HeaderComponent } from './header.component';
import { BehaviorSubject } from 'rxjs';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let authenticationServiceMock : any;
  let routerMock : any;

  beforeEach(() => {
    authenticationServiceMock = {
      checkIfAdminIsConnected : jest.fn(),
      logOut: jest.fn(),
      isUserConnected : new BehaviorSubject<boolean>(false),
      isAdminConnected : new BehaviorSubject<boolean>(false)
    };
    component = new HeaderComponent(authenticationServiceMock, routerMock);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should ngOnInit', () => {
    component.ngOnInit();
    expect(authenticationServiceMock.checkIfAdminIsConnected).toBeCalled();
  });

  it('should logOut', async () => {
    const SwalFireMock = jest.spyOn(Swal, 'fire').mockResolvedValue({
      isConfirmed: true,
    } as SweetAlertResult);
  
    const SwalMixinMock = jest.spyOn(Swal, 'mixin').mockReturnValue({
      toast : true,
      fire: jest.fn(),
      didOpen : jest.fn((toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      )
    } as any); 
  
    component.logOut();
  
    // Attendre la résolution de la promesse dans la méthode logOut
    await Promise.resolve();
  
    expect(SwalFireMock).toHaveBeenCalled();
  
    expect(SwalMixinMock).toHaveBeenCalled();
  
    expect(component.isUserConnected).toBeFalsy();
    expect(component.isAdminConnected).toBeFalsy();
  });


  it('should not logOut', async () => {
    const SwalFireMock = jest.spyOn(Swal, 'fire').mockResolvedValue({
      isDenied: true,
    } as SweetAlertResult);
  
    const SwalMixinMock = jest.spyOn(Swal, 'mixin').mockReturnValue({
      fire: jest.fn(),
    } as any);
  
    component.logOut();
  
    // Attendre la résolution de la promesse dans la méthode logOut
    await Promise.resolve();
  
    expect(component.isUserConnected).toBeFalsy();
    expect(component.isAdminConnected).toBeFalsy();
  });



});
