import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, Subscription, of } from 'rxjs';
import { subscribeOn } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/services/authentication.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isUserConnected: boolean = false;
  isUserConnectedSubscription!: Subscription;
  isAdminConnected: boolean = false;
  isAdminConnectedSubscription!: Subscription;

  constructor(private authenticationService: AuthenticationService, private router: Router) { }

  ngOnInit(): void {
    this.authenticationService.checkIfAdminIsConnected();
    this.isUserConnectedSubscription = this.authenticationService.isUserConnected.subscribe((val) => this.isUserConnected = val);
    this.isAdminConnectedSubscription = this.authenticationService.isAdminConnected.subscribe((val) => {
      this.isAdminConnected = val;
    }
    );
  }

  logOut() {

    Swal.fire({
      title: '<small>Voulez-vous se déconnecter?</small>',
      showDenyButton: false,
      showCancelButton: true,
      confirmButtonColor: '#17a2b8',
      cancelButtonText: 'Annuler',
      confirmButtonText: 'Confirmer',
      denyButtonText: `Non`,
    }).then((result) => {

      if (result.isConfirmed) {
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 300,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        })
        Toast.fire({
          icon: 'success',
          title: 'Vous êtes déconnecté',
        }).then(() => {
          this.authenticationService.logOut();
          this.isUserConnected = false;
          this.isAdminConnected = false;
          this.router.navigateByUrl('')
        })
      } else if (result.isDenied) {
        //this.router.navigateByUrl('');
      }
    })
    // this.authenticationService.logOut();
    // this.isUserConnected=false;
    // this.isAdminConnected=false;

  }

}
