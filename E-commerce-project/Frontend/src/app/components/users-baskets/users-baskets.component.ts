import { Component, OnInit } from '@angular/core';
import { Basket } from 'src/app/models/basket.model';
import { BasketService } from 'src/app/services/basket.service';

@Component({
  selector: 'app-users-baskets',
  templateUrl: './users-baskets.component.html',
  styleUrls: ['./users-baskets.component.css']
})
export class UsersBasketsComponent implements OnInit {

  userEmail!:string;
  baskets!: Basket[];
  showDetailsState : boolean = false;
  basketItemsToShow!:any;
  basketToShowTotalPrice!:number;
  basketToShowStates!:boolean;
  basketToShowComment!:string;

  constructor(private basketService:BasketService) { }

  ngOnInit(): void {

    let email  = localStorage.getItem('username');
    if (email!=null) {
      this.userEmail = email;
    }

    this.basketService.getUserBaskets(this.userEmail).subscribe(
      (data) => {
        this.baskets=data;  
      }
    )
  }

  showDetails(basket:Basket){
    this.basketItemsToShow=basket.basketItems;
    this.showDetailsState=true;
    this.basketToShowTotalPrice = 0;
    for (let i = 0; i < basket.basketItems.length; i++) {
      this.basketToShowTotalPrice += basket.basketItems[i].product.productPrice * basket.basketItems[i].quantity;
    }
    this.basketToShowStates=basket.checkedOut;
    this.basketToShowComment=basket.comment;
  }

  hideDetails(){
    this.showDetailsState=false;
  }

}
