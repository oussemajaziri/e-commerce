import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersBasketsComponent } from './users-baskets.component';
import { SchedulerLike, of } from 'rxjs';
import { Basket } from 'src/app/models/basket.model';

describe('UsersBasketsComponent', () => {
  let component: UsersBasketsComponent;
  let basketServiceMock : any;

  beforeEach(() => {
    const basket = new Basket();
    basket.id=1;
    basket.basketItems=[{id:1,quantity:1}];
    basket.checkedOut=false;
    basket.createdDate = new Date();
    basket.user={id:11,firstName:'foulen'};
    const baskets: any[] | SchedulerLike = [];
    baskets.push(basket);
    basketServiceMock = {
      getUserBaskets:jest.fn(()=> of(baskets))
    }
    component = new UsersBasketsComponent(basketServiceMock);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should ngOnInit', () => {
    component.ngOnInit();
    expect(component.baskets).toBeDefined();
  });

  it('should showDetails', () => {
    const basket = new Basket();
    basket.id=1;
    basket.basketItems=[{product:{id:10,productPrice :3},quantity:1,}];
    basket.checkedOut=false;
    basket.createdDate = new Date();
    component.showDetails(basket);
    expect(component.showDetails).toBeTruthy();
    expect(component.basketToShowTotalPrice).toBe(3);
  });

  it('should hideDetails', () => {
    component.hideDetails();
    expect(component.showDetailsState).toBeFalsy();
  });


});
