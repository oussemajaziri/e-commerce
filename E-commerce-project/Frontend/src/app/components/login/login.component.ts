import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm!: FormGroup;

  constructor(private fb: FormBuilder, private authenticationService: AuthenticationService, private router:Router) { }

  ngOnInit(): void {

    this.loginForm = this.fb.group({
      email: '',
      password: ''
    })
  }

  login(x: any) {
    this.authenticationService.authenticate(x).subscribe(
      data => {
        //console.log("data :", data);
        this.router.navigateByUrl('');
      } ,error => {
        //console.log("The error is : ",error);
        alert("Vérifier votre email et/ou votre mot de passe");     
      }
    )
  }

}
