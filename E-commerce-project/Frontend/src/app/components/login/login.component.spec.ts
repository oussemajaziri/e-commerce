import { of } from 'rxjs';
import { LoginComponent } from './login.component';
import { FormBuilder } from '@angular/forms';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fbMock : FormBuilder;
  let authenticationServiceMock : any;
  let routerMock : any;

  beforeEach(() => {
    fbMock = new FormBuilder();
    authenticationServiceMock = {
      authenticate: jest.fn(() => of({
        user: {
          id: 1
        }
      }))
    };
    routerMock = {
      navigateByUrl : jest.fn()
    }
    component = new LoginComponent(fbMock,authenticationServiceMock,routerMock);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should ngOnInit', () => {
    component.ngOnInit();
    expect(component.loginForm).toBeDefined();
  });


  it('should login', () => {
    let x : any;
    component.login(x);
    expect(routerMock.navigateByUrl).toHaveBeenCalledWith('');
  });

});
