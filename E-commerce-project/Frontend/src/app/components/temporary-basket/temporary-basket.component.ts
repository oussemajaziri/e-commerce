import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { from } from 'rxjs';
import { concatMap, map } from 'rxjs/operators';
import { Basket } from 'src/app/models/basket.model';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { BasketService } from 'src/app/services/basket.service';
import { ProductService } from 'src/app/services/product.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-temporary-basket',
  templateUrl: './temporary-basket.component.html',
  styleUrls: ['./temporary-basket.component.css']
})
export class TemporaryBasketComponent implements OnInit {


  temporaryBasket: any[] = [];
  temporaryBasketItems: any[] = [];
  totalPrice: number = 0;
  basket!: Basket;

  showMessage!: boolean;
  message!: string;
  goToLogin!: boolean;

  constructor(private productService: ProductService, private basketService: BasketService, private router: Router, private authenticationService: AuthenticationService, private changeDetectorRef: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.loadBasket();
    
  }

  loadBasket() {
    let localBasket = localStorage.getItem('basket');
    if (localBasket != null) {
      this.temporaryBasket = JSON.parse(localBasket);

      this.basket = new Basket();
      this.basket.basketItems = [];

      const basketItemsObservable = from(this.temporaryBasket);

      basketItemsObservable.pipe(
        concatMap((basketItem) => {
          return this.productService.getProductById(basketItem.id).pipe(
            map((data) => {
              let indexBasketItem = this.temporaryBasket.indexOf(basketItem);
              let prodId = basketItem.id;
              let product: any = data.productName;
              let unitPrice: any = data.productPrice;
              let quantity = basketItem.quantity;
              let order = { indexBasketItem, prodId, product, unitPrice, quantity };
              this.totalPrice = this.totalPrice + (unitPrice * quantity);
              this.temporaryBasketItems.push(order);

              let item = {
                'product': data,
                'quantity': basketItem.quantity
              }
              this.basket.basketItems.push(item);
            })
          );
        })
      ).subscribe();
     
      // for (let i = 0; i < this.temporaryBasket.length; i++) {
      //   this.productService.getProductById(this.temporaryBasket[i].id).subscribe(
      //     (data) => {
      //       let indexBasketItem = i;
      //       let prodId = this.temporaryBasket[i].id;
      //       let product: any = data.productName;
      //       let unitPrice: any = data.productPrice;
      //       let quantity = this.temporaryBasket[i].quantity;
      //       let order = { indexBasketItem, prodId, product, unitPrice, quantity };
      //       this.totalPrice = this.totalPrice + (unitPrice * quantity);
      //       this.temporaryBasketItems.push(order);

      //       let item = {
      //         'product': data,
      //         'quantity': this.temporaryBasket[i].quantity
      //       }
      //       this.basket.basketItems.push(item);
      //     }
      //   );
      // }



      // this.basket = new Basket();
      // this.basket.basketItems = [];
      // if (this.temporaryBasket.length > 0) {
      //   for (let i = 0; i < this.temporaryBasket.length; i++) {
      //     let item = {
      //       'product': this.productService.getProductById(this.temporaryBasket[i].id).subscribe(
      //         (data) => {
      //           item.product = data;
      //         }
      //       ),
      //       'quantity': this.temporaryBasket[i].quantity
      //     }
      //     this.basket.basketItems.push(item);
      //   }
      // }
      this.changeDetectorRef.detectChanges();
    }
  }

  addBasket() {
    Swal.fire({
      title: '<small>voulez vous valider votre panier?</small>',
      showDenyButton: false,
      showCancelButton: true,
      confirmButtonColor: '#17a2b8',
      cancelButtonText: 'Annuler',
      confirmButtonText: 'Confirmer',
      denyButtonText: `Non`,
    }).then((result) => {

      if (result.isConfirmed) {
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 300,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        })
        Toast.fire({
          icon: 'success',
          title: 'Envoie en cours',
        }).then(() => {
          this.authenticationService.getConnectUser().subscribe(
            (data) => {
              this.basket.user = data;
              if (this.basket.user != null) {
                console.log("this.basket ", this.basket);
                
                this.basketService.addBasket(this.basket).subscribe(
                  (data) => {
                    if (data.type == "basket") {
                      this.showMessage = true;
                      this.message = "Votre commande est envoyé !";
                      this.temporaryBasket = [];
                      this.temporaryBasketItems = [];
                      this.totalPrice = 0;
                      localStorage.removeItem('basket');
                      //alert("Votre panier est envoyé !")
                    } else {
                      let prods = "";
                      for (let i = 0; i < data.res.length; i++) {
                        if (prods == "") {
                          prods += " " + data.res[i].productName;
                        } else {
                          prods += " et " + data.res[i].productName;
                        }
                      }
                      this.showMessage = true;
                      this.message = 'Les quantités que vous avez commandé de(s) produit(s) : ' + prods + " ne sont plus disponibles!";
                      //alert('Les quantités que vous avez commandé des produits : ' + prods + " ne sont plus disponibles!")
                    }
                  }
                )
              } else {
                this.showMessage = true;
                this.goToLogin = true;
                this.message = 'Connectez vous pour envoyer votre panier !';
                //alert('Connectez vous pour envoyer votre panier !');
                //this.router.navigateByUrl('login');
              }
            }
          );
        })
      } else if (result.isDenied) {
        //this.router.navigateByUrl('');
      }
    })
  }

  deleteTemporaryBasket() {
    this.temporaryBasket = [];
    this.temporaryBasketItems = [];
    this.totalPrice = 0;
    localStorage.removeItem('basket');
  }

  deleteTempororyItem(indexBasketItem: number) {

    this.temporaryBasketItems.splice(indexBasketItem, 1);

    this.temporaryBasket.splice(indexBasketItem, 1);
    localStorage.removeItem('basket');

    let basketData = JSON.stringify(this.temporaryBasket);
    localStorage.setItem('basket', basketData);

    setTimeout(() => {
      if (this.temporaryBasketItems.length > 0) {
        this.totalPrice = 0;
        for (let i = 0; i < this.temporaryBasketItems.length; i++) {
          this.temporaryBasketItems[i].indexBasketItem = i;
          this.totalPrice += (this.temporaryBasketItems[i].unitPrice * this.temporaryBasketItems[i].quantity);
          this.changeDetectorRef.detectChanges();
        }
      } else {
        localStorage.removeItem('basket');
      }
    }, 0)
    this.changeDetectorRef.detectChanges();
  }


  modifyQuantityBasketItem(indexBasketItem: number) {
    this.changeDetectorRef.detectChanges();
    let availableQuantity!: number;
    this.productService.getProductById(this.temporaryBasketItems[indexBasketItem].prodId).subscribe(
      (data) => {
        availableQuantity = data.productQuantity;
        let quantities: number[] = [];
        for (let i = 0; i < availableQuantity; i++) {
          quantities.push(i + 1);
        }
        let quantityOptions = quantities.map(q => `<option>${q}</option>`).join('');
        Swal.fire({
          title: '<small>Selectionner la quantité souhaitée</small>',
          html:
            `<select id="quantitySelect"> ` +
            quantityOptions +
            `</select>`,
          showDenyButton: false,
          showCancelButton: true,
          confirmButtonColor: '#17a2b8',
          cancelButtonText: 'Annuler',
          confirmButtonText: 'Confirmer',
          denyButtonText: `Non`,
          didOpen: () => {
            const selectElement = document.getElementById('quantitySelect');
            if (selectElement) {
              selectElement.style.cursor = 'pointer';
              selectElement.style.minWidth = '3.5rem';
              selectElement.style.minHeight = '1.75rem';
              selectElement.style.textAlign = 'center';
            }
          }
        }).then((result) => {

          if (result.isConfirmed) {
            const quantitySelect = document.getElementById('quantitySelect') as HTMLSelectElement;
            const selectedQuantity = quantitySelect ? parseInt(quantitySelect.value) : 0;
            const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 300,
              timerProgressBar: true,
              didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })
            Toast.fire({
              icon: 'success',
              title: 'Le produit a été ajouté au panier',
            }).then(() => {
              this.temporaryBasketItems[indexBasketItem].quantity = selectedQuantity;
              this.temporaryBasket[indexBasketItem].quantity = selectedQuantity;
              localStorage.removeItem('basket');
              let basketData = JSON.stringify(this.temporaryBasket);
              localStorage.setItem('basket', basketData);
              this.totalPrice = 0;
              for (let i = 0; i < this.temporaryBasketItems.length; i++) {
                this.totalPrice += (this.temporaryBasketItems[i].unitPrice * this.temporaryBasketItems[i].quantity);
              }
              this.changeDetectorRef.detectChanges();
            })
          } else if (result.isDenied) {
            //this.router.navigateByUrl('temporarybasket');
          }
        })

      }
    );

  }

  closeMessageWindow() {
    this.showMessage = false;
  }

  goToLogine() {
    this.router.navigateByUrl('login');
  }

}
