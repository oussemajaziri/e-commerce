import { Basket } from 'src/app/models/basket.model';
import { TemporaryBasketComponent } from './temporary-basket.component';
import { of } from 'rxjs';

describe('TemporaryBasketComponent', () => {
  let component: TemporaryBasketComponent;
  let productServiceMock: any;
  let basketServiceMock: any;
  let routerMock: any;
  let authenticationServiceMock: any;
  let changeDetectorRefMock: any;

  const localStorageMock = (function () {
    let store: any = {};

    return {
      getItem(key: any) {
        return store[key];
      },

      setItem(key: any, value: any) {
        store[key] = value;
      },

      clear() {
        store = {};
      },

      removeItem(key: any) {
        delete store[key];
      },

      getAll() {
        return store;
      },
    };
  })();

  Object.defineProperty(window, "localStorage", { value: localStorageMock });

  const setLocalStorage = (id: any, data: any) => {
    window.localStorage.setItem(id, JSON.stringify(data));
  };

  beforeEach(() => {
    productServiceMock = {
      getProductById: jest.fn()
      
    };
    changeDetectorRefMock = {
      detectChanges: jest.fn()
    };
    basketServiceMock = {
      addBasket: jest.fn()
    };
    authenticationServiceMock = {
      getConnectUser: jest.fn()
    };
    routerMock = {
      navigateByUrl: jest.fn()
    };
    component = new TemporaryBasketComponent(productServiceMock, basketServiceMock, routerMock, authenticationServiceMock, changeDetectorRefMock)
  });

  afterEach(() => {
    jest.clearAllMocks();
    localStorageMock.clear();
  })

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should ngOnInit', () => {
    const loadBasketSpy = jest.spyOn(component, 'loadBasket');
    component.ngOnInit();
    expect(loadBasketSpy).toHaveBeenCalled();
  });

  it('should loadBasket', () => {
    const mockProduct = { id: 1, productName: 'Product 1', productPrice: 10 };
    jest.spyOn(productServiceMock, 'getProductById').mockReturnValue(of(mockProduct));
    component.temporaryBasket= [{ id: 1, quantity: 1 }];
    component.basket=new Basket();
    const basket = new Basket();
    basket.id = 1;
    basket.checkedOut = false;
    basket.createdDate = new Date();
    basket.user = { id: 1 };
    basket.basketItems = [{ id: 1, quantity: 1 }];
    const mockKey = "basket";
    const mockJson = basket;
    setLocalStorage(mockKey, JSON.stringify(mockJson));
    component.loadBasket();
    expect(changeDetectorRefMock.detectChanges).toHaveBeenCalled();
  });


  it('should deleteTemporaryBasket', () => {
    component.deleteTemporaryBasket();
    expect(component.temporaryBasket).toEqual([]);
    expect(component.temporaryBasketItems).toEqual([]);
    expect(component.totalPrice).toBe(0);
  });

  it('should deleteTempororyItem', () => {
    component.temporaryBasketItems = [{ id: 1, quantity: 1, unitPrice: 3 }, { id: 2, quantity: 1, unitPrice: 5 }];
    component.temporaryBasket = [{ id: 1, quantity: 1, unitPrice: 3 }, { id: 2, quantity: 1, unitPrice: 5 }];
    jest.useFakeTimers(); // Activez les fake timers.
    component.deleteTempororyItem(1);
    // Avancez dans le temps pour que setTimeout s'exécute.
    jest.runAllTimers();
    expect(changeDetectorRefMock.detectChanges).toHaveBeenCalled();
  });

  it('should deleteTempororyItem no more items', () => {
    component.temporaryBasketItems = [{ id: 1, quantity: 1, unitPrice: 3 }];
    component.temporaryBasket = [{ id: 1, quantity: 1, unitPrice: 3 }];
    jest.useFakeTimers(); // Activez les fake timers.
    component.deleteTempororyItem(0);
    // Avancez dans le temps pour que setTimeout s'exécute.
    jest.runAllTimers();
    localStorageMock.removeItem=jest.fn();
    setTimeout(() => {
      localStorageMock.removeItem = jest.fn();
      expect(localStorageMock.removeItem).toHaveBeenCalled();
  }, 0);
  });

  it('should modifyQuantityBasketItem', () => {
    const mockProduct = { id: 1, productName: 'Product 1', productPrice: 5,productQuantity:11};
    jest.spyOn(productServiceMock, 'getProductById').mockReturnValue(of(mockProduct));
    component.temporaryBasketItems = [{ id: 1, quantity: 1, unitPrice: 5 }];
    component.modifyQuantityBasketItem(0);
    expect(productServiceMock.getProductById).toHaveBeenCalled();
  });

  it('should closeMessageWindow', () => {
    component.closeMessageWindow();
    expect(component.showMessage ).toBeFalsy();
  });

  it('should goToLogine', () => {
    component.goToLogine();
    expect(routerMock.navigateByUrl ).toHaveBeenCalled();
  });
 
});
