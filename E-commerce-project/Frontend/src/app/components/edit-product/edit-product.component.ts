import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { concatMap, map, tap } from 'rxjs/operators';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

  prodId!: number;
  currentProduct!: any;

  productForm!: FormGroup;
  selectedImg!: File;
  imagePreview: any;
  product: any;


  constructor(private activatedRoute: ActivatedRoute, private fb: FormBuilder, private productService: ProductService, private router: Router) { }

  ngOnInit(): void {

    this.prodId = + this.activatedRoute.snapshot.params.id;

    this.productForm = this.fb.group({
      productName: '',
      productQuantity: '',
      productPrice: ''
    })


    this.productService.getProductById(this.prodId).subscribe(
      (data) => {
        this.currentProduct = data;
        this.productForm.patchValue({
          productName: this.currentProduct.productName,
          productQuantity: this.currentProduct.productQuantity,
          productPrice: this.currentProduct.productPrice,
        })
      }
    )

  }

  onFileSelected(img: any) {
    this.selectedImg = img.target.files[0];
    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = reader.result as string
    };
    reader.readAsDataURL(this.selectedImg);
  }

  updateProduct() {
    const formData = new FormData();
    formData.append('productId', this.currentProduct.id);
    formData.append('productName', this.productForm.value.productName);
    formData.append('productQuantity', this.productForm.value.productQuantity);
    formData.append('productPrice', this.productForm.value.productPrice);
    formData.append('productImage', this.selectedImg!=null? this.selectedImg : new File([], "empty-file"));

    this.productService.updateProduct(formData).subscribe(
      (data) => {
        this.product = data;
        this.router.navigateByUrl("products");
      }
    )

  }

  cancelUpdateProduct(){
    this.router.navigateByUrl("products");
  }

}
