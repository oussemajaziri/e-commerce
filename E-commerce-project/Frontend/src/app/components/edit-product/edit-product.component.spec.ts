import { FormBuilder } from '@angular/forms';
import { EditProductComponent } from './edit-product.component';
import { of } from 'rxjs';

describe('EditProductComponent', () => {
  let component: EditProductComponent;
  let activatedRouteMock: any;
  let fbMock: FormBuilder;
  let productServiceMock: any;
  let routerMock: any;

  beforeEach(() => {
    activatedRouteMock = {
      snapshot: {
        params: {
          id: 1
        }
      }
    };
    fbMock = new FormBuilder();
    productServiceMock = {
      getProductById: jest.fn(() => of({
        productName: 'p1',
        productQuantity: 10,
        productPrice: 1,
      })),
      updateProduct: jest.fn(() => of({
        productName: 'p1',
        productQuantity: 10,
        productPrice: 1,
      }))
    };
    routerMock = {
      navigateByUrl: jest.fn()
    }
    component = new EditProductComponent(activatedRouteMock, fbMock, productServiceMock, routerMock);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should ngOnInit', () => {
    component.ngOnInit();
    expect(component.prodId).toBeDefined();
    expect(productServiceMock.getProductById).toHaveBeenCalled();
  });

  it('should onFileSelected', () => {
    const img = {
      target: {
        files: [new Blob()]
      }
    }
    component.onFileSelected(img);
    expect(component.selectedImg).toBeDefined();
  });

  it('should updateProduct', () => {
    component.ngOnInit();
    component.updateProduct();
    expect(productServiceMock.updateProduct).toBeDefined();
  });

  it('should cancelUpdateProduct', () => {
    component.cancelUpdateProduct();
    expect(routerMock.navigateByUrl).toHaveBeenCalledWith("products");
  });


});
