import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpHeaders
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authenticationService: AuthenticationService) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    if (request.url.includes(`${this.authenticationService.URL}/auth`)) {
      return next.handle(request);
    }

    if (request.url.includes(`${this.authenticationService.URL}/products`)) {
      return next.handle(request);
    }


    let token : string = this.authenticationService.getToken();
    token = this.authenticationService.getToken().slice(1, -1);
    
    const headers = new HttpHeaders().append('Authorisation', `Bearer ${token}`);

    //const modifiedRequest = request.clone({ headers : headers });
    const modifiedRequest = request.clone({ setHeaders: { Authorization: `Bearer ${token}` }});
    
    return next.handle(modifiedRequest);
  }
}
