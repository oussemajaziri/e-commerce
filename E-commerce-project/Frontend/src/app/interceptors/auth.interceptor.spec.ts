import { AuthInterceptor } from './auth.interceptor';

describe('AuthInterceptor', () => {
  let interceptor : AuthInterceptor;
  let authenticationServiceMock : any;

  beforeEach(() => {
    authenticationServiceMock = {
      URL : 'http://localhost:8080/api/v1',
      getToken : jest.fn()
    }
    interceptor = new AuthInterceptor(authenticationServiceMock);
  });

  it('should be created', () => {
    expect(interceptor).toBeTruthy();
  });

  it('should intercept auth case', () => {
    const request : any = {
      url : 'http://localhost:8080/api/v1/auth'
    };
    const next : any = {
      handle : jest.fn()
    };
    interceptor.intercept(request,next);
    expect(next.handle).toHaveBeenCalled();
  });

  it('should intercept products case', () => {
    const request : any = {
      url : 'http://localhost:8080/api/v1/products'
    };
    const next : any = {
      handle : jest.fn()
    };
    interceptor.intercept(request,next);
    expect(next.handle).toHaveBeenCalled();
  });

  it('should intercept general case', () => {
    const fakeToken = jest.spyOn(authenticationServiceMock,'getToken').mockReturnValue('fakeToken');
    const request : any = {
      url : 'http://localhost:8080/api/v1/baskets',
      clone : jest.fn()
    };
    const next : any = {
      handle : jest.fn()
    };
    interceptor.intercept(request,next);
    expect(next.handle).toHaveBeenCalled();
  });

});
