package com.jaz.ecommerce.controllers;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.security.Key;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;

import com.jaz.ecommerce.dto.ProductDto;
import com.jaz.ecommerce.services.impl.ProductServiceImpl;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

@SpringBootTest
@AutoConfigureMockMvc
public class ProductControllerTest {

	@Autowired
	private MockMvc mockMvc;
	 
	@MockBean
	ProductServiceImpl productServiceImpl;
		
	public String getAdminToken() throws Exception {
	    String email = "jaz@mail.com";
	    Map<String, Object> claims = new HashMap<String, Object>();
	    List<String> roles = new ArrayList<>();
	    roles.add("ADMIN");
	    claims.put("roles", roles);
	    String token = 
	    		Jwts.builder()
	            .setClaims(claims)
	            .setSubject(email)
	            .setIssuer("http://localhost:8080/api/v1/auth/register")
	            .setIssuedAt(new Date(System.currentTimeMillis()))
	            .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 24))
	            .signWith(getSignInKey(), SignatureAlgorithm.HS256)
	            .compact();
	    System.out.println("this token : "+ token);
	    return token;
	}
	
	private Key getSignInKey() {
		String SECRET_KEY = "404E635266556A586E3272357538782F413F4428472B4B6250645367566B5970";
		byte[] keyBytes = Decoders.BASE64.decode(SECRET_KEY);
		return Keys.hmacShaKeyFor(keyBytes);
	}
	
	@Test
	void testSaveProduct () throws Exception{
		String token = this.getAdminToken();
		ProductDto prod = new ProductDto("prod-1", 10, 25d,"test.jpeg" );
		byte[] content = new byte[]{(byte) 0xFF, (byte) 0xD8, (byte) 0xFF, (byte) 0xE0};
		MockMultipartFile file = new MockMultipartFile("productImage", "test.jpeg", "image/jpeg", content);
		when(this.productServiceImpl.saveProduct("prod-1", 10, 25d, file)).thenReturn(prod);
		this.mockMvc.perform(multipart("/api/v1/admin/products")
		            .file(file)
		            .header("Authorization", "Bearer " + token)
		            .param("productName", "prod-1")
		            .param("productQuantity", "10")
		            .param("productPrice", "25")
		            .contentType(MediaType.MULTIPART_FORM_DATA_VALUE))
		            .andExpect(status().isOk());
	}
	
	@Test
	void testUpdateProduct () throws Exception{
		String token = this.getAdminToken();
		ProductDto prod = new ProductDto(1L,"prod-1", 10, 25d,"new-image.jpeg");
		byte[] content = new byte[]{(byte) 0xFF, (byte) 0xD8, (byte) 0xFF, (byte) 0xE0};
		MockMultipartFile file = new MockMultipartFile("productImage", "new-image.jpeg", "image/jpeg", content);
		when(this.productServiceImpl.updateProduct(1L,"prod-1", 10, 25d, file)).thenReturn(prod);
		this.mockMvc.perform(multipart("/api/v1/admin/products/update")
		            .file(file)
		            .header("Authorization", "Bearer " + token)
		            .param("productId", "1")
		            .param("productName", "prod-1")
		            .param("productQuantity", "10")
		            .param("productPrice", "25")
		            .contentType(MediaType.MULTIPART_FORM_DATA_VALUE))
		            .andExpect(status().isOk());
	}
	
	/*
	@Test
	void testGetProductImg () throws Exception{
	    String token = this.getAdminToken();
	    byte[] content = new byte[]{(byte) 0xFF, (byte) 0xD8, (byte) 0xFF, (byte) 0xE0};
	    when(Files.readAllBytes(Paths.get(System.getProperty("user.home") + "/images/demo/"+"image.jpeg"))).thenReturn(content);
	    this.mockMvc.perform(get("/api/v1/products/images/{image}", "image.jpeg")
	            .header("Authorization", "Bearer " + token))
	            .andExpect(status().isOk());
	}
	*/
	
	@Test
	void testGetProductById () throws Exception{
	    String token = this.getAdminToken();
	    ProductDto prod = new ProductDto("prod-1", 10, 25d,"new-image.jpeg");
	    when(this.productServiceImpl.getProductById(1L)).thenReturn(prod);
	    this.mockMvc.perform(get("/api/v1/products/{prodId}",1)
	            .header("Authorization", "Bearer " + token))
	            .andExpect(status().isOk());
	}
	
	@Test
	void testGetProductByIdForBasket () throws Exception{
	    String token = this.getAdminToken();
	    ProductDto prod = new ProductDto("prod-1", 10, 25d,"new-image.jpeg");
	    when(this.productServiceImpl.getProductByIdForBasket(1L)).thenReturn(prod);
	    this.mockMvc.perform(get("/api/v1/products/basket/{prodId}",1)
	            .header("Authorization", "Bearer " + token))
	            .andExpect(status().isOk());
	}
	
	@Test
	void testGetAllProducts () throws Exception{
	    String token = this.getAdminToken();
	    ProductDto prod1 = new ProductDto(1L,"prod-1", 10, 25d,"new-image.jpeg");
	    ProductDto prod2 = new ProductDto(2L,"prod-2", 10, 25d,"new-image.jpeg");
	    List<ProductDto> productList = new ArrayList<>();
	    productList.add(prod1);
	    productList.add(prod2);
	    PageRequest pageRequest = PageRequest.of(0,3);
	    Page<ProductDto> res = new PageImpl<>(productList, pageRequest, productList.size());
	    when(this.productServiceImpl.getAllProducts(0)).thenReturn(res);
	    this.mockMvc.perform(get("/api/v1/products/page/{page}",0)
	            .header("Authorization", "Bearer " + token))
	            .andExpect(status().isAccepted());
	}
	
	
	@Test
	void testGetAllProductsNoProductCase () throws Exception{
	    String token = this.getAdminToken();
	    List<ProductDto> productList = new ArrayList<>();
	    PageRequest pageRequest = PageRequest.of(0,3);
	    Page<ProductDto> res = new PageImpl<>(productList, pageRequest, productList.size());
	    when(this.productServiceImpl.getAllProducts(0)).thenReturn(res);
	    this.mockMvc.perform(get("/api/v1/products/page/{page}",0)
	            .header("Authorization", "Bearer " + token))
	            .andExpect(status().isAccepted());
	}
	
	@Test
	void testDeleteProduct () throws Exception{
	    String token = this.getAdminToken();
	    ProductDto prod = new ProductDto("prod-1", 10, 25d,"new-image.jpeg");
	    when(this.productServiceImpl.getProductByIdForBasket(1L)).thenReturn(prod);
	    this.mockMvc.perform(delete("/api/v1/products/{prodId}",1)
	            .header("Authorization", "Bearer " + token))
	            .andExpect(status().isOk());
	} 

}
