package com.jaz.ecommerce.services;


import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.web.multipart.MultipartFile;

import com.jaz.ecommerce.dto.ProductDto;

@SpringBootTest
@TestPropertySource("classpath:application-test.properties")
public class ProductServiceImplTest {
	
	@Autowired
	ProductService productService;
	
	@Test
	@Sql(scripts = "/drop_prod_table.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testSaveProduct() throws IOException {
	    byte[] content = new byte[]{(byte) 0xFF, (byte) 0xD8, (byte) 0xFF, (byte) 0xE0};
	    MultipartFile file = new MockMultipartFile("file", "test.jpg", "image/jpeg", content);
		assertEquals("p1", this.productService.saveProduct("p1", 10, 25d, file).getProductName());
	}
	
	@Test
	void testSaveProductIsNotImageCase() throws IOException {
	    byte[] content = "Contenu du fichier PDF".getBytes();
	    MultipartFile file = new MockMultipartFile("file", "test.pdf", "application/pdf", content);
	    assertThrows(IllegalStateException.class, () -> {this.productService.saveProduct("p1", 10, 25d, file);});
	}
	
	@Test
	void testSaveProductIsEmptyImgCase() throws IOException {
	    byte[] content = new byte[0];
	    MultipartFile file = new MockMultipartFile("file", "test.jpg", "image/jpeg", content);
	    assertThrows(IllegalStateException.class, () -> {this.productService.saveProduct("p1", 10, 25d, file);});
	}
	
	@Test
	@Sql(scripts = "/prod_data.sql",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "/drop_prod_table.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testUpdateProduct() throws IOException {
	    byte[] content = new byte[]{(byte) 0xFF, (byte) 0xD8, (byte) 0xFF, (byte) 0xE0};
	    MultipartFile file = new MockMultipartFile("file", "test.jpg", "image/jpeg", content);
		assertEquals("p1", this.productService.updateProduct(1L,"p1", 10, 25d, file).getProductName());
	}
	
	@Test
	@Sql(scripts = "/prod_data.sql",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "/drop_prod_table.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testUpdateProductWithoutImgUpdating() throws IOException {
	    byte[] content = new byte[0];
	    MultipartFile file = new MockMultipartFile("file", "test.jpg", "image/jpeg", content);
		assertEquals("p1", this.productService.updateProduct(1L,"p1", 10, 25d, file).getProductName());
	}
	
	
	@Test
	@Sql(scripts = "/prod_data.sql",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "/drop_prod_table.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testGetProductById() throws IOException {
		assertEquals("prod1", this.productService.getProductById(1L).getProductName());
	}
	
	@Test
	@Sql(scripts = "/prod_data.sql",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "/drop_prod_table.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testGetProductByIdNotFoundCase() throws IOException {
		 ProductDto productDto = this.productService.getProductById(10L);
		 assertNull(productDto);
	}
	
	@Test
	@Sql(scripts = "/prod_data.sql",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "/drop_prod_table.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testGetProductByIdForBasket() throws IOException {
		assertEquals("prod1", this.productService.getProductByIdForBasket(1L).getProductName());
	}
	
	@Test
	@Sql(scripts = "/prod_data.sql",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "/drop_prod_table.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testGetAllProducts() throws IOException {
		assertEquals(1, this.productService.getAllProducts(0).getTotalPages());
	}
	
	@Test
	@Sql(scripts = "/prod_data.sql",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "/drop_prod_table.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testDeletProductById() throws IOException {
		 assertDoesNotThrow(() -> {this.productService.deleteProductById(1L);});
	}
	
	
	@Test
	@Sql(scripts = "/prod_data.sql",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "/drop_prod_table.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testUpdateProductQuantity() {
		assertEquals(99, this.productService.updateProductQuantity(99,1L).getProductQuantity());
	}
	
	@Test
	@Sql(scripts = "/prod_data.sql",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "/drop_prod_table.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testUpdateProductQuantityNullCase() {
		assertEquals(null, this.productService.updateProductQuantity(99,10L));
	}


}
