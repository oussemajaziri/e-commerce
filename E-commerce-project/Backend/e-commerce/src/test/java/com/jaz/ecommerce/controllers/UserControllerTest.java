package com.jaz.ecommerce.controllers;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.security.Key;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jaz.ecommerce.dto.UserDto;
import com.jaz.ecommerce.entities.Role;
import com.jaz.ecommerce.services.impl.UserServiceImpl;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.Matchers.*;


@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	 
	@MockBean
	UserServiceImpl userServiceImpl;
	
	private static final String SECRET_KEY = "404E635266556A586E3272357538782F413F4428472B4B6250645367566B5970";
	
	public String getToken() throws Exception {
	    String email = "jaz@mail.com";
	    Map<String, Object> claims = new HashMap<String, Object>();
	    List<String> roles = new ArrayList<>();
	    roles.add("ADMIN");
	    claims.put("roles", roles);
	    String token = 
	    		Jwts.builder()
	            .setClaims(claims)
	            .setSubject(email)
	            .setIssuer("http://localhost:8080/api/v1/auth/register")
	            .setIssuedAt(new Date(System.currentTimeMillis()))
	            .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 24))
	            .signWith(getSignInKey(), SignatureAlgorithm.HS256)
	            .compact();
	    System.out.println("this token : "+ token);
	    return token;
	}
	
	private Key getSignInKey() {
		byte[] keyBytes = Decoders.BASE64.decode(SECRET_KEY);
		return Keys.hmacShaKeyFor(keyBytes);
	}
	
	@Test
	void testSignUp() throws Exception {
		UserDto user = new UserDto(1L,"username","userlastname","user@mail.com","111","999",null,null);
		when(this.userServiceImpl.saveUser(user)).thenReturn(user);
		ObjectMapper objectMapper = new ObjectMapper();
		String userJson = objectMapper.writeValueAsString(user);
		this.mockMvc.perform(post("/api/v1/signup")
			.content(userJson)
			.contentType(MediaType.APPLICATION_JSON))
        	.andExpect(status().isCreated());
	}
	
	
	@Test
	void testSignUpNullUserCase() throws Exception {
		UserDto user = new UserDto(1L,"username","userlastname","user@mail.com","111","999",Role.USER,null);
		when(this.userServiceImpl.saveUser(user)).thenReturn(null);
		ObjectMapper objectMapper = new ObjectMapper();
		String userJson = objectMapper.writeValueAsString(user);
		this.mockMvc.perform(post("/api/v1/signup")
			.content(userJson)
			.contentType(MediaType.APPLICATION_JSON))
        	.andExpect(status().isNotAcceptable());
	}
	
	@Test
	void testGetUserById() throws Exception {
		String token = this.getToken();
		UserDto user = new UserDto(1L,"username","userlastname","user@mail.com","111","999",Role.USER,null);
		when(this.userServiceImpl.getUserById(1L)).thenReturn(user);
		this.mockMvc.perform(get("/api/v1/users/{userId}",1).header("Authorization", "Bearer " + token))
			.andExpect(status().isOk());
	}
	
	@Test
	void testGetUserByIdNullCase() throws Exception {
		String token = this.getToken();
		when(this.userServiceImpl.getUserById(1L)).thenReturn(null);
		this.mockMvc.perform(get("/api/v1/users/{userId}",1).header("Authorization", "Bearer " + token))
			.andExpect(status().isNoContent())
			.andExpect(jsonPath("$").doesNotExist());
	}
	
	@Test
	void testGetUserByEmail() throws Exception {
		String token = this.getToken();
		UserDto user = new UserDto(1L,"userfirstname","userlastname","user@mail.com","111","999",Role.USER,null);
		when(this.userServiceImpl.getUserByEmail("user@mail.com")).thenReturn(user);
		this.mockMvc.perform(get("/api/v1/users/username/{userEmail}","user@mail.com").header("Authorization", "Bearer " + token))
			.andExpect(status().isOk());
	}
	
	@Test
	void testGetUserByEmailNullCase() throws Exception {
		String token = this.getToken();
		when(this.userServiceImpl.getUserByEmail("user@mail.com")).thenReturn(null);
		this.mockMvc.perform(get("/api/v1/users/username/{userEmail}","user@mail.com").header("Authorization", "Bearer " + token))
			.andExpect(status().isNoContent())
			.andExpect(jsonPath("$").doesNotExist());
	}

	
	@Test
	void testGetAllUsers() throws Exception {
		String token = this.getToken();
		UserDto user1 = new UserDto(1L,"user1","xxx","user1@mail.com","111","999",Role.USER,null);
		UserDto user2 = new UserDto(1L,"user2","yyy","user2@mail.com","222","333",Role.USER,null);
		List<UserDto> users = new ArrayList<>();
		users.add(user1);
		users.add(user2);
		when(this.userServiceImpl.getAllUsers()).thenReturn(users);
		this.mockMvc.perform(get("/api/v1/users").header("Authorization", "Bearer " + token))
			.andExpect(status().isOk())
        	.andExpect(jsonPath("$[0].firstName", is("user1")));

	}
}
