package com.jaz.ecommerce.services;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.jaz.ecommerce.dto.UserDto;
import com.jaz.ecommerce.entities.Role;
import com.jaz.ecommerce.entities.User;
import com.jaz.ecommerce.helpers.ModelMapperConverter;

@SpringBootTest
@TestPropertySource("classpath:application-test.properties")
public class UserServiceImplTest {
	
	@Autowired
	UserService userService;
	
	@Test
	@Sql(scripts = "/drop_user_table.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testSaveUser() {
		User user = new User(1L,"foulen","ben foulen","foulen@mail.com","111","999",Role.USER,null);
		this.userService.saveUser(ModelMapperConverter.map(user, UserDto.class));
		assertEquals("foulen", this.userService.getUserById(1l).getFirstName());
	}
	
	@Test
	@Sql(scripts = "/user_data.sql",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "/drop_user_table.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testSaveUserNullCase() {
		User user = new User(2L,"foulen","ben foulen","jaz@mail.com","111","999",Role.USER,null);
		this.userService.saveUser(ModelMapperConverter.map(user, UserDto.class));
		assertEquals(null, this.userService.getUserById(2l));
	}
	
	@Test
	@Sql(scripts = "/user_data.sql",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "/drop_user_table.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testGetUserById() {
		assertEquals("ouss", this.userService.getUserById(1l).getFirstName());
	}
	
	@Test
	@Sql(scripts = "/user_data.sql",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "/drop_user_table.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testGetUserByEmail() {
		assertEquals("ouss", this.userService.getUserByEmail("jaz@mail.com").getFirstName());
	}
	
	@Test
	@Sql(scripts = "/user_data.sql",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "/drop_user_table.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testGetAllUsers() {
		assertEquals("ouss", this.userService.getAllUsers().get(0).getFirstName());
	}

}
