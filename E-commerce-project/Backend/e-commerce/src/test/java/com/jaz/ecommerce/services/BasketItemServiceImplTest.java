package com.jaz.ecommerce.services;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.jaz.ecommerce.dto.BasketItemDto;
import com.jaz.ecommerce.entities.BasketItem;
import com.jaz.ecommerce.entities.Product;
import com.jaz.ecommerce.helpers.ModelMapperConverter;

@SpringBootTest
@TestPropertySource("classpath:application-test.properties")
public class BasketItemServiceImplTest {


	@Autowired
	BasketItemService basketItemService;
	
	@Autowired
	ProductService productService;
	
	@Test
	@Sql(scripts = "/prod_data.sql",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "/drop_basket-item.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(scripts = "/drop_prod_table.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testSaveBasketItem() {
		Product prod = ModelMapperConverter.map(this.productService.getProductById(1L),Product.class);
		BasketItem item = new BasketItem(1l,prod,5);
		this.basketItemService.saveBasketItem(ModelMapperConverter.map(item, BasketItemDto.class));
		assertEquals("prod1", this.basketItemService.getBasketItemById(1L).getProduct().getProductName());
	}
	
	@Test
	@Sql(scripts = "/basket_item.sql",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "/drop_basket-item.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(scripts = "/drop_prod_table.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testGetAllBasketItems() {
		List<BasketItem> items =ModelMapperConverter.mapAll(this.basketItemService.getAllBasketItems(),BasketItem.class);
		assertEquals(2, items.size());
	}
	
	@Test
	@Sql(scripts = "/basket_item.sql",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "/drop_basket-item.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(scripts = "/drop_prod_table.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testDeletProductById() throws IOException {
		 assertDoesNotThrow(() -> {this.basketItemService.deleteBasketItemById(10L);});
	}
}
