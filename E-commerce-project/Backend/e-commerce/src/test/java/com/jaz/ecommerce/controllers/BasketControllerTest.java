package com.jaz.ecommerce.controllers;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.security.Key;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jaz.ecommerce.dto.BasketDto;
import com.jaz.ecommerce.dto.BasketItemDto;
import com.jaz.ecommerce.dto.UserDto;
import com.jaz.ecommerce.entities.User;
import com.jaz.ecommerce.helpers.ModelMapperConverter;
import com.jaz.ecommerce.services.impl.BasketItemServiceImpl;
import com.jaz.ecommerce.services.impl.BasketServiceImpl;
import com.jaz.ecommerce.services.impl.BasketServiceImpl.ResponseAddBasket;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

@SpringBootTest
@AutoConfigureMockMvc
public class BasketControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	 
	@MockBean
	BasketServiceImpl basketServiceImpl;
	
	@MockBean
	BasketItemServiceImpl basketItemServiceImpl;
	
	public String getAdminToken() throws Exception {
	    String email = "jaz@mail.com";
	    Map<String, Object> claims = new HashMap<String, Object>();
	    List<String> roles = new ArrayList<>();
	    roles.add("ADMIN");
	    claims.put("roles", roles);
	    String token = 
	    		Jwts.builder()
	            .setClaims(claims)
	            .setSubject(email)
	            .setIssuer("http://localhost:8080/api/v1/auth/register")
	            .setIssuedAt(new Date(System.currentTimeMillis()))
	            .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 24))
	            .signWith(getSignInKey(), SignatureAlgorithm.HS256)
	            .compact();
	    System.out.println("this token : "+ token);
	    return token;
	}
	
	public String getUserToken() throws Exception {
	    String email = "foulen@mail.com"; 
	    Map<String, Object> claims = new HashMap<>();
	    // Create a list of roles with the desired structure
	    List<Map<String, String>> roles = new ArrayList<>();
	    Map<String, String> role = new HashMap<>();
	    role.put("authority", "USER");
	    roles.add(role);
	    // Add roles and other claims to the payload
	    claims.put("sub", email);
	    claims.put("roles", roles);
	    String token =
	            Jwts.builder()
	                    .setClaims(claims)
	                    .setIssuer("http://localhost:8080/api/v1/auth/register")
	                    .setIssuedAt(new Date(System.currentTimeMillis()))
	                    .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 24))
	                    .signWith(getSignInKey(), SignatureAlgorithm.HS256)
	                    .compact();

	    System.out.println("this token : " + token);
	    return token;
	}
	
	private Key getSignInKey() {
		String SECRET_KEY = "404E635266556A586E3272357538782F413F4428472B4B6250645367566B5970";
		byte[] keyBytes = Decoders.BASE64.decode(SECRET_KEY);
		return Keys.hmacShaKeyFor(keyBytes);
	}
	
	
	@Test
	void testSaveBasket () throws Exception{
		String token = this.getUserToken();
		BasketDto basket = new BasketDto();	
        User user = new User();
        user.setId(1L);
        basket.setUser(ModelMapperConverter.map(user, UserDto.class));
        ResponseAddBasket res = new ResponseAddBasket();
        res.setRes(basket);
        res.setType("basket");
        when(this.basketServiceImpl.saveBasket(basket)).thenReturn(res);
		ObjectMapper objectMapper = new ObjectMapper();
		String basketJson = objectMapper.writeValueAsString(basket);
		this.mockMvc.perform(post("/api/v1/baskets")
		            .header("Authorization", "Bearer " + token)
		            .content(basketJson)
					.contentType(MediaType.APPLICATION_JSON))
		        	.andExpect(status().isOk());
	}
	
	
	@Test
	void testGetBasketById () throws Exception{
		String token = this.getAdminToken();
		BasketDto basket = new BasketDto();	
        User user = new User();
        user.setId(1L);
        basket.setUser(ModelMapperConverter.map(user, UserDto.class));;
        when(this.basketServiceImpl.getBasketById(1L)).thenReturn(basket);
		ObjectMapper objectMapper = new ObjectMapper();
		String basketJson = objectMapper.writeValueAsString(basket);
		this.mockMvc.perform(get("/api/v1/baskets/{basketId}",1L)
		            .header("Authorization", "Bearer " + token)
		            .content(basketJson)
					.contentType(MediaType.APPLICATION_JSON))
		        	.andExpect(status().isOk());
	}
	
	
	@Test
	void testGetAllBaskets () throws Exception{
		String token = this.getAdminToken();
		BasketDto basket1 = new BasketDto();
		BasketDto basket2 = new BasketDto();
        User user = new User();
        user.setId(1L);
        basket1.setUser(ModelMapperConverter.map(user, UserDto.class));
        basket2.setUser(ModelMapperConverter.map(user, UserDto.class));
        List<BasketDto> baskets = new ArrayList<>();
        baskets.add(basket1);
        baskets.add(basket2);
        when(this.basketServiceImpl.getAllBaskets()).thenReturn(baskets);
        this.mockMvc.perform(get("/api/v1/baskets").header("Authorization", "Bearer " + token))
					.andExpect(status().isOk());
	}
	
	
	@Test
	void testValidateBasketById () throws Exception{
		String token = this.getAdminToken();
		BasketDto basket = new BasketDto();
        User user = new User();
        user.setId(1L);
        basket.setUser(ModelMapperConverter.map(user, UserDto.class));
        when(this.basketServiceImpl.validateBasket(1L)).thenReturn(basket);
        this.mockMvc.perform(post("/api/v1/baskets/{basketId}",1)
        			.header("Authorization", "Bearer " + token))
					.andExpect(status().isOk());
	}
	
	@Test
	void testSaveBasketItem () throws Exception{
		String token = this.getUserToken();
		BasketItemDto item = new BasketItemDto();	
        when(this.basketItemServiceImpl.saveBasketItem(item)).thenReturn(item);
		ObjectMapper objectMapper = new ObjectMapper();
		String itemJson = objectMapper.writeValueAsString(item);
		this.mockMvc.perform(post("/api/v1/baskets/basketitems")
		            .header("Authorization", "Bearer " + token)
		            .content(itemJson)
					.contentType(MediaType.APPLICATION_JSON))
		        	.andExpect(status().isCreated());
	}
	
	@Test
	void testRefuseBasket () throws Exception{
		String token = this.getAdminToken();
		BasketDto basket = new BasketDto();
        User user = new User();
        user.setId(1L);
        basket.setUser(ModelMapperConverter.map(user, UserDto.class));
        when(this.basketServiceImpl.refuseBasket(1L,"comment")).thenReturn(basket);
        this.mockMvc.perform(post("/api/v1/baskets/{comment}/{basketId}","comment",1)
        			.header("Authorization", "Bearer " + token))
					.andExpect(status().isOk());
	}

}
