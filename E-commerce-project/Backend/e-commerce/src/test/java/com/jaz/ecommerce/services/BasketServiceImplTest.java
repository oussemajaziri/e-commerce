package com.jaz.ecommerce.services;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.jaz.ecommerce.dto.BasketDto;
import com.jaz.ecommerce.dto.BasketItemDto;
import com.jaz.ecommerce.dto.ProductDto;
import com.jaz.ecommerce.dto.UserDto;
import com.jaz.ecommerce.helpers.ModelMapperConverter;

@SpringBootTest
@TestPropertySource("classpath:application-test.properties")
public class BasketServiceImplTest {

	@Autowired
	BasketService  basketService;
	
	@Autowired
	UserService UserService;
	
	@Autowired
	ProductService productService;
	
	
	@Test
	@Sql(scripts = "/prod_data.sql",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "/user_data.sql",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "/drop_jointure-basket-basket_items.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(scripts = "/drop_basket_table.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(scripts = "/drop_user_table.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(scripts = "/drop_basket-item.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(scripts = "/drop_prod_table.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testBasketServiceImplAllMethods() throws Exception {
		BasketDto basket = new BasketDto();
		basket.setId(1L);
		basket.setUser( ModelMapperConverter.map(this.UserService.getUserById(20L), UserDto.class)  );
		BasketItemDto item1 = new BasketItemDto();
		item1.setProduct(ModelMapperConverter.map(this.productService.getProductById(1L), ProductDto.class));
		item1.setQuantity(1);
		List<BasketItemDto> itemsList = new ArrayList<>();
		itemsList.add(item1);
		basket.setBasketItems(itemsList);
		this.basketService.saveBasket(basket);
		
		assertEquals(1, this.basketService.getBasketById(1L).getBasketItems().size());
		
		assertEquals("foulen", this.basketService.getBasketByUserEmail("foulen@mail.com").get(0).getUser().getFirstName());
		
		assertEquals("prod1", this.basketService.getAllBaskets().get(0).getBasketItems().get(0).getProduct().getProductName());
		
		BasketItemDto item2 = new BasketItemDto();
		item2.setProduct(ModelMapperConverter.map(this.productService.getProductById(1L), ProductDto.class));
		item2.setQuantity(1);
		this.basketService.addItemToBasket(1L, item2);
		assertEquals(2, this.basketService.getBasketById(1L).getBasketItems().size());
		
		assertEquals(true, this.basketService.validateBasket(1L).getCheckedOut());
		
		assertEquals(false, this.basketService.refuseBasket(1L,"comment").getCheckedOut());
		
		assertDoesNotThrow(() -> {this.basketService.deleteBasketById(1L);});
	}
	
	
	@Test
	@Sql(scripts = "/prod_data.sql",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "/user_data.sql",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "/drop_jointure-basket-basket_items.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(scripts = "/drop_basket_table.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(scripts = "/drop_user_table.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(scripts = "/drop_basket-item.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(scripts = "/drop_prod_table.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testSaveBasketNoAvailableQuantity() throws Exception {
		BasketDto basket = new BasketDto();
		basket.setId(2L);
		basket.setUser( ModelMapperConverter.map(this.UserService.getUserById(20L), UserDto.class)  );
		BasketItemDto item1 = new BasketItemDto();
		item1.setProduct(ModelMapperConverter.map(this.productService.getProductById(1L), ProductDto.class));
		item1.setQuantity(100);
		List<BasketItemDto> itemsList = new ArrayList<>();
		itemsList.add(item1);
		basket.setBasketItems(itemsList);
		this.basketService.saveBasket(basket);
		assertEquals("no-basket", this.basketService.saveBasket(basket).type);
	}
	
	


}
