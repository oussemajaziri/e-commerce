package com.jaz.ecommerce.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jaz.ecommerce.entities.User;

public interface UserRepo  extends JpaRepository<User, Long> {
	
	Optional<User> findByEmail(String email);

}
