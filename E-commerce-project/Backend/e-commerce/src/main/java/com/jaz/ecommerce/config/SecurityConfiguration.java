package com.jaz.ecommerce.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


import lombok.RequiredArgsConstructor;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)   //pour travailler avec les annotation d'autorisation
@RequiredArgsConstructor
public class SecurityConfiguration {
	
	private final JwtAuthenticationFilter jwtAuthFilter;
	private final AuthenticationProvider authenticationProvider;

	@Bean
	public SecurityFilterChain securityFilterChain (HttpSecurity http) throws Exception {
		
		http
			.cors()
			.and()
			.csrf()
			.disable()
			// pour permettre l'affichage de H2
			.headers()
		    .frameOptions()
		    .disable()
		    .and()
			//
			.authorizeHttpRequests()
				//.requestMatchers("/api/auth/**") for spring boot 3
				.antMatchers("/api/v1/signup/**","/api/v1/auth/**","/api/v1/products/page/{page}","/api/v1/products/images/{image}","/api/v1/products/{prodId}","/h2-console/**").permitAll()
				//.antMatchers("/api/v1/demo/**","/api/v1/products/**").hasAnyAuthority("ADMIN")
				.anyRequest()
				.authenticated()
				//.permitAll()
			.and()
			.sessionManagement()
			.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and()
			.authenticationProvider(authenticationProvider)
			.addFilterBefore(jwtAuthFilter,UsernamePasswordAuthenticationFilter.class);
		
		return http.build();
	}

}
