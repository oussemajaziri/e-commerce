package com.jaz.ecommerce.dto;

import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jaz.ecommerce.entities.User;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BasketDto {
	
	private Long id;	
	private UserDto user;
	//@JsonIgnore
	//@ToString.Exclude
	private List<BasketItemDto> basketItems;	
	private LocalDateTime createdDate;
	private LocalDateTime modifiedDate;
	private Boolean checkedOut;
	private String comment;
	
	public BasketDto(UserDto user, List<BasketItemDto> basketItems, LocalDateTime createdDate, LocalDateTime modifiedDate,
			Boolean checkedOut) {
		super();
		this.user = user;
		this.basketItems = basketItems;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.checkedOut = checkedOut;
	}
	
}
