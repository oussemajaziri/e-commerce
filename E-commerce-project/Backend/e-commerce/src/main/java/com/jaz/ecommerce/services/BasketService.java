package com.jaz.ecommerce.services;

import java.io.IOException;
import java.util.List;

import com.jaz.ecommerce.dto.BasketDto;
import com.jaz.ecommerce.dto.BasketItemDto;
import com.jaz.ecommerce.services.impl.BasketServiceImpl.ResponseAddBasket;

public interface BasketService {

	ResponseAddBasket saveBasket(BasketDto basketDto) throws IOException;

	BasketDto getBasketById(Long id);

	List<BasketDto> getAllBaskets();
	
	void deleteBasketById(Long basketId);
	
	Object addItemToBasket(Long basketId, BasketItemDto basketItem) throws IOException;
	
	List<BasketDto> getBasketByUserEmail(String userEmail);
	
	BasketDto validateBasket (Long id)throws IOException;
	
	BasketDto refuseBasket (Long id, String comment)throws IOException;
}
