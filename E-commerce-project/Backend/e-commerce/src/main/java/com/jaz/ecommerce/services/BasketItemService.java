package com.jaz.ecommerce.services;

import java.util.List;
import io.jsonwebtoken.io.IOException;

import com.jaz.ecommerce.dto.BasketItemDto;

public interface BasketItemService {

	
	BasketItemDto saveBasketItem(BasketItemDto basketItemDto) throws IOException;

	BasketItemDto getBasketItemById(Long id);

	List<BasketItemDto> getAllBasketItems();
	
	void deleteBasketItemById(Long basketItemId);
}
