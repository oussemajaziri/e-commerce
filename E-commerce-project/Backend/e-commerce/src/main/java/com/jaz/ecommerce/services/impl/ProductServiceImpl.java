package com.jaz.ecommerce.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import com.jaz.ecommerce.dto.ProductDto;
import com.jaz.ecommerce.entities.Product;
import com.jaz.ecommerce.helpers.ModelMapperConverter;
import com.jaz.ecommerce.repositories.ProductRepo;
import com.jaz.ecommerce.services.ProductService;

import static org.springframework.http.MediaType.IMAGE_JPEG;
import static org.springframework.http.MediaType.IMAGE_PNG;
import static org.springframework.http.MediaType.IMAGE_GIF;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.file.Paths.get;
import java.util.Arrays;
import java.util.Optional;

import static java.nio.file.Files.*;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductRepo productRepo;

	public static final Path DIRECTORY = get(System.getProperty("user.home") + "/images/demo/");

	private void isImage(MultipartFile file) {
		if (!Arrays.asList(IMAGE_JPEG.toString(), IMAGE_PNG.toString(), IMAGE_GIF.toString())
				.contains(file.getContentType())) {
			throw new IllegalStateException("File must be an image [" + file.getContentType() + "]");
		}
	}

	private void isFileEmpty(MultipartFile file) {
		if (file.isEmpty()) {
			throw new IllegalStateException("Cannot upload empty file [" + file.getSize() + "]");
		}
	} 

	@Override
	public ProductDto saveProduct(String productName, Integer productQuantity, Double productPrice, MultipartFile file)
			throws IOException {

		// Check if image is not empty
		isFileEmpty(file);

		// Check If file is an image
		isImage(file);

		if (!exists(DIRECTORY))
			createDirectories(DIRECTORY);
		String filename = String.format("%s", file.getOriginalFilename());
		Path fileStorage = get(String.valueOf(DIRECTORY), filename).toAbsolutePath().normalize();

		// Get the bytes of the file
		byte[] fileBytes = file.getBytes();

		// Write the bytes to the file
		Files.write(fileStorage, fileBytes);

		return ModelMapperConverter.map(
				this.productRepo
						.save(new Product(productName, productQuantity,productPrice,file.getOriginalFilename())),
				ProductDto.class);

	}

	@Override
	public ProductDto updateProduct(Long prodId, String productName, Integer productQuantity, Double productPrice,
			MultipartFile file) throws IOException {

		if (!file.isEmpty()) {
			// Check if image is not empty
			isFileEmpty(file);

			// Check If file is an image
			isImage(file);

			if (!exists(DIRECTORY))
				createDirectories(DIRECTORY);
			String filename = String.format("%s", file.getOriginalFilename());
			Path fileStorage = get(String.valueOf(DIRECTORY), filename).toAbsolutePath().normalize();

			// Get the bytes of the file
			byte[] fileBytes = file.getBytes();

			// Write the bytes to the file
			Files.write(fileStorage, fileBytes);

			return ModelMapperConverter.map(this.productRepo
					.save(new Product(prodId, productName,productQuantity, productPrice, file.getOriginalFilename())),
					ProductDto.class);

		}

		Optional<Product> product = this.productRepo.findById(prodId);
		String image = product.isPresent()? product.get().getProductImage():null;

		return ModelMapperConverter.map(
				this.productRepo.save(new Product(prodId, productName,productQuantity, productPrice, image)),
				ProductDto.class);

	}

	@Override
	public ProductDto getProductById(Long id) {

		Optional<Product> product = this.productRepo.findById(id);
		if (product.isPresent()) {
			product.get().setProductImage(UriComponentsBuilder.newInstance().scheme("http").host("localhost").port(8080)
					.path("api/v1/products/images/" + product.get().getProductImage()).toUriString());
		}
		return ModelMapperConverter.map(product, ProductDto.class);
	}
	
	@Override
	public ProductDto getProductByIdForBasket(Long id) {
		return ModelMapperConverter.map(this.productRepo.findById(id), ProductDto.class);
	}

	@Override
	public Page<ProductDto> getAllProducts(int page) {

		Page<Product> products = this.productRepo
				.findAllByOrderByIdDesc(PageRequest.of(page, 3, Sort.by("id").ascending()));

		products.forEach(p -> {
			p.setProductImage(UriComponentsBuilder.newInstance().scheme("http").host("localhost").port(8080)
					.path("api/v1/products/images/" + p.getProductImage()).toUriString());
		});

		return products.map(entity -> ModelMapperConverter.map(entity, ProductDto.class));
	}

	@Override
	public void deleteProductById(Long prodId) {
		this.productRepo.deleteById(prodId);
	}

	@Override
	public ProductDto updateProductQuantity(Integer newQuantity, Long id) {
		Optional<Product> product = this.productRepo.findById(id);
		if (product.isPresent()) {
			product.get().setProductQuantity(newQuantity);
			return ModelMapperConverter.map(this.productRepo.save(product.get()), ProductDto.class);
		}
		return null;
	}

}
