package com.jaz.ecommerce.services;

import java.util.List;

import com.jaz.ecommerce.dto.UserDto;

public interface UserService {

	UserDto saveUser(UserDto user);
	UserDto getUserById(Long userId);
	UserDto getUserByEmail(String email);
	List<UserDto> getAllUsers();
}