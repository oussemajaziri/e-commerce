package com.jaz.ecommerce.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jaz.ecommerce.dto.BasketItemDto;
import com.jaz.ecommerce.entities.BasketItem;
import com.jaz.ecommerce.helpers.ModelMapperConverter;
import com.jaz.ecommerce.repositories.BasketItemRepo;
import com.jaz.ecommerce.services.BasketItemService;

import io.jsonwebtoken.io.IOException;

@Service
public class BasketItemServiceImpl implements BasketItemService{
	
	@Autowired
	BasketItemRepo basketItemRepo;

	@Override
	public BasketItemDto saveBasketItem(BasketItemDto basketItemDto) throws IOException {
		return ModelMapperConverter.map(this.basketItemRepo.save(ModelMapperConverter.map(basketItemDto, BasketItem.class)), BasketItemDto.class);
	}

	@Override
	public BasketItemDto getBasketItemById(Long id) {
		Optional<BasketItem> basketItem = this.basketItemRepo.findById(id);
		return ModelMapperConverter.map(basketItem, BasketItemDto.class);
	}

	@Override
	public List<BasketItemDto> getAllBasketItems() {
		List<BasketItem> basketItems = this.basketItemRepo.findAll();
		return ModelMapperConverter.mapAll(basketItems, BasketItemDto.class);
	}

	@Override
	public void deleteBasketItemById(Long basketItemId) {
		this.basketItemRepo.deleteById(basketItemId);
	}

}
