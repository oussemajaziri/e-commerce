package com.jaz.ecommerce.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jaz.ecommerce.dto.UserDto;
import com.jaz.ecommerce.services.UserService;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin
public class UserController {

	@Autowired
	UserService userService;

	@PreAuthorize("permitAll()")
	@PostMapping("/signup")
	public ResponseEntity<Object> signup(@RequestBody UserDto userDto) {
		UserDto user = userService.saveUser(userDto);
		if (user == null) {
			return new ResponseEntity<>("Cette adresse e-mail est déjà utilisée", HttpStatus.NOT_ACCEPTABLE);
		}
		return new ResponseEntity<>(user, HttpStatus.CREATED);
	}

	@PreAuthorize("permitAll()")
	@GetMapping("/users/{userId}")
	public ResponseEntity<UserDto> getUserById(@PathVariable Long userId) {
		if (userService.getUserById(userId) == null) {
			return new ResponseEntity<>(userService.getUserById(userId), HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(userService.getUserById(userId), HttpStatus.OK);
	}
	
	@PreAuthorize("permitAll()")
	@GetMapping("/users/username/{userEmail}")
	public ResponseEntity<UserDto> getUserByEmail(@PathVariable String userEmail) {
		if (userService.getUserByEmail(userEmail) == null) {
			return new ResponseEntity<>(userService.getUserByEmail(userEmail), HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(userService.getUserByEmail(userEmail), HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('ADMIN')")
	@GetMapping("/users")
	public List<UserDto> getAllUsers() {
		return this.userService.getAllUsers();
	}
	
	
}
