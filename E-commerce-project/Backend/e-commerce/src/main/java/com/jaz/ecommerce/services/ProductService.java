package com.jaz.ecommerce.services;

import java.io.IOException;

import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import com.jaz.ecommerce.dto.ProductDto;



public interface ProductService {
	
	ProductDto saveProduct(String productName, Integer productQuantity,Double productPrice, MultipartFile file) throws IOException;

	ProductDto updateProduct(Long prodId, String productName, Integer productQuantity,Double productPrice, MultipartFile file) throws IOException;

	ProductDto getProductById(Long id);
	
	ProductDto getProductByIdForBasket(Long id);

	Page<ProductDto> getAllProducts(int page);
	
	void deleteProductById(Long prodId);
	
	ProductDto updateProductQuantity(Integer newQuantity,Long id);

}
