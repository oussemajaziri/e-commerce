package com.jaz.ecommerce.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jaz.ecommerce.dto.UserDto;
import com.jaz.ecommerce.entities.User;
import com.jaz.ecommerce.helpers.ModelMapperConverter;
import com.jaz.ecommerce.repositories.UserRepo;
import com.jaz.ecommerce.services.UserService;


@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	UserRepo userRepo;

	
	//private PasswordEncoder passwordEncoder;

	@Override
	public UserDto saveUser(UserDto userDto) {
        if (this.getUserByEmail(userDto.getEmail()) != null) return null;
        //userDto.setPassword(passwordEncoder.encode(userDto.getPassword()));
        userDto.setPassword(userDto.getPassword());
        //userDto.setRole(this.roleRepo.findById(2L).orElse(null));
        return ModelMapperConverter.map(this.userRepo.save(ModelMapperConverter.map(userDto, User.class)), UserDto.class);
	}

	@Override
	public UserDto getUserById(Long userId) {
		Optional<User> user = this.userRepo.findById(userId);
		return ModelMapperConverter.map(user,UserDto.class);
	}

	@Override
	public UserDto getUserByEmail(String email) {
		Optional<User> user = this.userRepo.findByEmail(email);
		return ModelMapperConverter.map(user,UserDto.class);
	}

	@Override
	public List<UserDto> getAllUsers() {
		return ModelMapperConverter.mapAll(this.userRepo.findAll(),UserDto.class);
	}

}
