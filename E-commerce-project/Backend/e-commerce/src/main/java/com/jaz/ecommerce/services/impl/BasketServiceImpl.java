package com.jaz.ecommerce.services.impl;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jaz.ecommerce.dto.BasketDto;
import com.jaz.ecommerce.dto.BasketItemDto;
import com.jaz.ecommerce.entities.Basket;
import com.jaz.ecommerce.entities.BasketItem;
import com.jaz.ecommerce.entities.Product;
import com.jaz.ecommerce.entities.User;
import com.jaz.ecommerce.helpers.ModelMapperConverter;
import com.jaz.ecommerce.repositories.BasketRepo;
import com.jaz.ecommerce.repositories.ProductRepo;
import com.jaz.ecommerce.repositories.UserRepo;
import com.jaz.ecommerce.services.BasketService;
import com.jaz.ecommerce.services.ProductService;

import lombok.Data;
import lombok.NoArgsConstructor;

@Service
public class BasketServiceImpl implements BasketService {

	@Autowired
	BasketRepo basketRepo;

	@Autowired
	UserRepo userRepo;

	@Autowired
	ProductRepo productRepo;

	@Autowired
	ProductService productService;
	
	@Data
	@NoArgsConstructor
	public static class ResponseAddBasket {
		public String type;
		public Object res;
	}
	
	
	
	  
	//test
	@Override
	public synchronized ResponseAddBasket saveBasket(BasketDto basketDto) throws IOException {
		
		Basket basket = ModelMapperConverter.map(basketDto, Basket.class);

		Optional<User> user = this.userRepo.findById(basket.getUser().getId());
		basket.setUser(user.isPresent() ? user.get() : null);

		basket.setCreatedDate(LocalDateTime.now());
	
		basket.setCheckedOut(null);
		
		List<Product> prods = new ArrayList<>();
		List<Integer> qnts = new ArrayList<>();
		
		List<Product> prodToUpdate = new ArrayList<>();
		List<Product> prodUnAvailable = new ArrayList<>();
		
		//insert prod quantity
		basket.getBasketItems().forEach(item -> {
			prods.add(item.getProduct());
			qnts.add(item.getQuantity());
		});
			
		//check quantities
		prods.forEach((p) -> {
			Integer availableQuantity = this.productService.getProductByIdForBasket(p.getId()).getProductQuantity();
			System.out.println("availableQuantity : " + availableQuantity);
			int q = qnts.get(prods.indexOf(p));
			System.out.println("quantity souhaitee : " + q);
			if (q <= availableQuantity ) {
				prodToUpdate.add(p);
			} else {
				prodUnAvailable.add(p);
			}
		});
		
		//update quantities
		if (prodUnAvailable.size() == 0) {
			prods.forEach(p -> {
				//System.out.println("prod to update :" + p);
				int q = qnts.get(prods.indexOf(p));
				this.productService.updateProductQuantity(p.getProductQuantity() - q, p.getId());
			});
			BasketDto basketAdded = ModelMapperConverter.map(this.basketRepo.save(basket), BasketDto.class);
			ResponseAddBasket res = new ResponseAddBasket();
			res.res=basketAdded;
			res.type = "basket";
			return res;
		} else {
			ResponseAddBasket res = new ResponseAddBasket();
			res.res=prodUnAvailable;
			res.type = "no-basket";
			return res;
		}
	}
	
	@Override
	public BasketDto getBasketById(Long id) {
		Optional<Basket> basket = this.basketRepo.findById(id);
		return ModelMapperConverter.map(basket, BasketDto.class);
	}

	@Override
	public List<BasketDto> getAllBaskets() {
		List<Basket> baskets = this.basketRepo.findByOrderByCreatedDateDesc();
		List<BasketDto> basketsDto = ModelMapperConverter.mapAll(baskets, BasketDto.class);
		return basketsDto;
	}

	@Override
	public void deleteBasketById(Long basketId) {
		this.basketRepo.deleteById(basketId);
	}

	// @Transactional
	@Override
	public Object addItemToBasket(Long basketId, BasketItemDto basketItemDto) throws IOException {
		Basket basket = ModelMapperConverter.map(this.getBasketById(basketId), Basket.class);
		System.out.println("old basket : " + basket);
		List<BasketItem> items = basket.getBasketItems();
		System.out.println("items : " + items);
		Optional<Product> prod = this.productRepo.findById(basketItemDto.getProduct().getId());
		BasketItem basketItem = new BasketItem();
		basketItem.setProduct(prod.isPresent() ? prod.get() : null);
		basketItem.setQuantity(basketItemDto.getQuantity());
		items.add(basketItem);
		basket.setBasketItems(items);
		System.out.println("new basket : " + basket);
		return this.saveBasket(ModelMapperConverter.map(basket, BasketDto.class));
	}

	@Override
	public List<BasketDto> getBasketByUserEmail(String userEmail) {
		return ModelMapperConverter.mapAll(basketRepo.findByUserEmailOrderByCreatedDateDesc(userEmail), BasketDto.class);
	}

	@Override
	public BasketDto validateBasket(Long id) throws IOException {
		BasketDto basket = this.getBasketById(id);
		basket.setCheckedOut(true);
		return ModelMapperConverter.map(this.basketRepo.save(ModelMapperConverter.map(basket, Basket.class)),BasketDto.class );
	}
	
	@Override
	public BasketDto refuseBasket(Long id, String comment) throws IOException {
		Basket basket = ModelMapperConverter.map( this.getBasketById(id),Basket.class);
		basket.setCheckedOut(false);
		basket.setComment(comment);
		//update quantities
		/*
		List<Product> prods = new ArrayList<>();
		List<Integer> qnts = new ArrayList<>();
	
		basket.getBasketItems().forEach(item -> {
			prods.add(item.getProduct());
			qnts.add(item.getQuantity());
		}); 
		
		prods.forEach(p -> {
			int q = qnts.get(prods.indexOf(p));
			this.productService.updateProductQuantity(p.getProductQuantity() + q, p.getId());
		});
		*/
		return ModelMapperConverter.map(this.basketRepo.save(ModelMapperConverter.map(basket, Basket.class)),BasketDto.class );
	}
	
}
