package com.jaz.ecommerce.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jaz.ecommerce.dto.BasketDto;
import com.jaz.ecommerce.dto.BasketItemDto;
import com.jaz.ecommerce.services.BasketItemService;
import com.jaz.ecommerce.services.BasketService;
import com.jaz.ecommerce.services.impl.BasketServiceImpl.ResponseAddBasket;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin
public class BasketController {

	@Autowired
	BasketService basketService;

	@Autowired
	BasketItemService basketItemService;

	//@PreAuthorize ("hasAuthority('ADMIN')")
	//@PreAuthorize ("hasAuthority('USER')")
	//@PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
	//@PreAuthorize("permitAll()")
	
	@PreAuthorize ("hasAuthority('USER')")
	@PostMapping("/baskets")
	ResponseEntity<?> addBasket(@RequestBody BasketDto basketDto) throws IOException {
		ResponseAddBasket res = this.basketService.saveBasket(basketDto);
		return new ResponseEntity<>(res, HttpStatus.OK);
	}

	@PreAuthorize ("hasAuthority('ADMIN')")
	@GetMapping("/baskets")
	ResponseEntity<List<BasketDto>> getAllBaskets() {
		List<BasketDto> baskets = this.basketService.getAllBaskets();
		return new ResponseEntity<>(baskets, HttpStatus.OK);
	}

	@PreAuthorize ("hasAuthority('USER')")
	@PostMapping("/baskets/basketitems")
	ResponseEntity<Object> addBasketItem(@RequestBody BasketItemDto basketItem) throws IOException {
		this.basketItemService.saveBasketItem(basketItem);
		return new ResponseEntity<>(basketItem, HttpStatus.CREATED);
	}

	@PreAuthorize ("hasAuthority('USER')")
	@PostMapping("/baskets/basketitem/{basketId}")
	ResponseEntity<Object> addItemToBasket(@RequestBody BasketItemDto basketItem, @PathVariable Long basketId)
			throws IOException {
		return new ResponseEntity<>(this.basketService.addItemToBasket(basketId, basketItem), HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
	@GetMapping("/baskets/{userEmail}")
	ResponseEntity<List<BasketDto>> getBasketsByUserEmail(@PathVariable String userEmail) {
		List<BasketDto> baskets = this.basketService.getBasketByUserEmail(userEmail);
		return new ResponseEntity<>(baskets, HttpStatus.OK);
	}
	
	@PreAuthorize ("hasAuthority('ADMIN')")
	@PostMapping("/baskets/{basketId}")
	ResponseEntity<?> validateBasket(@PathVariable Long basketId) throws IOException {
		return new ResponseEntity<>(this.basketService.validateBasket(basketId), HttpStatus.OK);
	}
	
	@PreAuthorize ("hasAuthority('ADMIN')")
	@PostMapping("/baskets/{comment}/{basketId}")
	ResponseEntity<?> refuseBasket(@PathVariable Long basketId,@PathVariable String comment) throws IOException {
		return new ResponseEntity<>(this.basketService.refuseBasket(basketId,comment), HttpStatus.OK);
	}

}
