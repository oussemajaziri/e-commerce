package com.jaz.ecommerce;



import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.jaz.ecommerce.authentication.AuthenticationService;
import com.jaz.ecommerce.authentication.RegisterRequest;
import com.jaz.ecommerce.entities.Role;



@SpringBootApplication
public class ECommerceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ECommerceApplication.class, args);
	}
	
	/*
	@Bean
	CommandLineRunner run(AuthenticationService authenticationService) {
		return args -> {
		
			authenticationService.register(new RegisterRequest("ouss","jaz","oussjaz@mail.com","123","58011115",Role.ADMIN));
			authenticationService.register(new RegisterRequest("foulen","ben foulen","foulen@mail.com","123","102020",Role.USER));
			authenticationService.register(new RegisterRequest("foulena","ben foulen","foulena@mail.com","123","302020",Role.USER));
			authenticationService.register(new RegisterRequest("leonel","messi","messi@mail.com","123","708090",Role.USER));
			authenticationService.register(new RegisterRequest("roger","fedrer","federer@mail.com","123","11111111",Role.USER));
		};
	}
	*/


}
