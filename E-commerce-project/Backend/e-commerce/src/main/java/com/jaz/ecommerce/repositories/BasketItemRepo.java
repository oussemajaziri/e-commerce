package com.jaz.ecommerce.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jaz.ecommerce.entities.BasketItem;

public interface BasketItemRepo extends JpaRepository<BasketItem, Long> {

}
