package com.jaz.ecommerce.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jaz.ecommerce.entities.Basket;

@Repository
public interface BasketRepo extends JpaRepository<Basket, Long> {

	List<Basket> findByUserEmailOrderByCreatedDateDesc(String userEmail);
	
	List<Basket> findByOrderByCreatedDateDesc();
}
