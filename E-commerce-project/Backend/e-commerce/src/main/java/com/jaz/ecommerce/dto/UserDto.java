package com.jaz.ecommerce.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jaz.ecommerce.entities.Role;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

	private Long id;
	private String firstName;
	private String lastName;
	private String email;
	private String password;
	private String phone;
	private Role role;
	@JsonIgnore
	@ToString.Exclude
	private List<BasketDto> baskets;
	
	public UserDto(String firstName, String lastName, String email, String password, String phone, Role role,List<BasketDto> baskets) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.phone = phone;
		this.role = role;
		this.baskets = baskets;
	}
	
}
