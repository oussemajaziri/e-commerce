package com.jaz.ecommerce.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductDto {

	private Long id;
	private String productName;
	private Integer productQuantity;
	private Double productPrice;
	private String productImage;


	public ProductDto(String productName, Integer productQuantity, Double productPrice , String productImage) {
		super();
		this.productName = productName;
		this.productQuantity = productQuantity;
		this.productPrice = productPrice;
		this.productImage = productImage;
	}

}
