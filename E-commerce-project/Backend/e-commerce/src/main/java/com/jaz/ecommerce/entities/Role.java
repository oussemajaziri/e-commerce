package com.jaz.ecommerce.entities;

public enum Role {
	
	USER,
	ADMIN

}