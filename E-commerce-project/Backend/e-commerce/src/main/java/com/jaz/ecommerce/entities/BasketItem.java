package com.jaz.ecommerce.entities;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table (name = "basket_item")
public class BasketItem {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/*
	@ManyToOne
	@JoinColumn(name = "basket_id", nullable = false)
	private Basket basket;
	*/
	@ManyToOne
	@JoinColumn(name = "product_id", nullable = false)
	private Product product;
	
	private Integer quantity;
}