package com.jaz.ecommerce.controllers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.jaz.ecommerce.dto.ProductDto;
import com.jaz.ecommerce.services.ProductService;

import static org.springframework.http.MediaType.IMAGE_PNG_VALUE;
import static org.springframework.http.MediaType.IMAGE_JPEG_VALUE;
import static org.springframework.http.MediaType.IMAGE_GIF_VALUE;


@RestController
@RequestMapping("/api/v1")
@CrossOrigin
public class ProductController {

	@Autowired
	ProductService productService;
	
	//@PreAuthorize ("hasAuthority('ADMIN')")
	//@PreAuthorize ("hasAuthority('USER')")
	//@PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
	//@PreAuthorize("permitAll()")
	
	
	@PreAuthorize ("hasAuthority('ADMIN')")
	@PostMapping("/admin/products")
	ProductDto saveProduct(@RequestParam String productName, @RequestParam Integer productQuantity,
			@RequestParam Double productPrice, @RequestParam MultipartFile productImage) throws IOException {
		return productService.saveProduct(productName, productQuantity, productPrice, productImage);
	}
	
	@PreAuthorize ("hasAuthority('ADMIN')")
	@PostMapping("/admin/products/update")
	ProductDto updateProduct(@RequestParam Long productId,@RequestParam String productName, @RequestParam Integer productQuantity,
			@RequestParam Double productPrice, @RequestParam MultipartFile productImage) throws IOException {
		return productService.updateProduct(productId,productName, productQuantity, productPrice, productImage);
	}

	@PreAuthorize("permitAll()")
	@GetMapping(path = "products/images/{image}", produces = { IMAGE_PNG_VALUE, IMAGE_JPEG_VALUE, IMAGE_GIF_VALUE })
	byte[] getProductImg(@PathVariable String image) throws IOException {
		return Files.readAllBytes(Paths.get(System.getProperty("user.home") + "/images/demo/" + image));
	}

	@PreAuthorize("permitAll()")
	@GetMapping("/products/{prodId}")
	ProductDto getProductById(@PathVariable Long prodId) {
		return productService.getProductById(prodId);
	}
	
	@PreAuthorize("permitAll()")
	@GetMapping("/products/basket/{prodId}")
	ProductDto getProductByIdForBasket(@PathVariable Long prodId) {
		return productService.getProductByIdForBasket(prodId);
	}
	
	@PreAuthorize("permitAll()")
	@GetMapping("/products/page/{page}")
	public ResponseEntity<Object> getAllProducts(@PathVariable int page) {
		Page<ProductDto> products=productService.getAllProducts(page);
		if (products.isEmpty()) {
			return new ResponseEntity<>("Pas de produits", HttpStatus.ACCEPTED);
		}
		return new ResponseEntity<>(products, HttpStatus.ACCEPTED);
	}

	@PreAuthorize ("hasAuthority('ADMIN')")
	@DeleteMapping("/products/{prodId}")
	public void deleteProduct(@PathVariable Long prodId) {
		productService.deleteProductById(prodId);
	}

}
