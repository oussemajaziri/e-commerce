package com.jaz.ecommerce.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BasketItemDto {

	private Long id;
	//private BasketDto basket;
	private ProductDto product;	
	private Integer quantity;
	
	public BasketItemDto(ProductDto product, Integer quantity) {
		super();
		this.product = product;
		this.quantity = quantity;
	}

}
